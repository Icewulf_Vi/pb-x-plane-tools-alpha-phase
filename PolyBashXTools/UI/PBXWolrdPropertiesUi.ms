rollout PBXWorldPropertiesUi "World Properties"
(
	local PropClass = PBXPropStr
	
	local vWorldZeroCordLat = (PropClass.getFileProp "WorldZeroCordLat")
	local vWorldZeroCordLon = (PropClass.getFileProp "WorldZeroCordLon")
	local vWorldZeroCordAlt = (PropClass.getFileProp "WorldZeroCordAlt")
	
	-------------------------------------------------------------------------------------------
	editText WorldZeroCordLat "Zero Point lat" text:vWorldZeroCordLat width:192 height:17
	editText WorldZeroCordLon "Zero Point lon" text:vWorldZeroCordLon width:192 height:17
	editText WorldZeroCordAlt "Zero Altitude" text:vWorldZeroCordAlt width:192 height:17
	-------------------------------------------------------------------------------------------


	on WorldZeroCordLat changed arg do
	(
		PropClass.SetFileProp "WorldZeroCordLat" arg
	)
	
	on WorldZeroCordLon changed arg do
	(
		PropClass.SetFileProp "WorldZeroCordLon" arg
	)
	
	on WorldZeroCordAlt changed arg do
	(
		PropClass.SetFileProp "WorldZeroCordAlt" arg
	)
)

