-- Main User Interface for PBX Tools 

rollout PBXTools "XTools" width:417 height:66
(
	fn RefreshPBXToolsRF LoadUiFiles TargetRolloutFloater =
	(
		-- Clear Rollout Floater
		for ro in TargetRolloutFloater.Rollouts do removeRollout ro TargetRolloutFloater
		
		-- AddMain Buttons to RF
		addRollout PBXTools TargetRolloutFloater
		
		-- LoadRolloutFiles
		for f in LoadUiFiles do 
		(
			addRollout (FileIn (PBXToolsInstallationPath + f)) TargetRolloutFloater
			PBXToolsLoadedRollout = undefined
		)
	)
	
	Fn GetUiFilesFromModules UiTag =
	(
		UiModulPropClass = PBXPropStr CFGFilePath:(PBXToolsInstallationPath + "\\Modules\\Modules.cfg")
		UiModules = UiModulPropClass.getfromcfg UiTag True
		
		return UiModules
	)
	
	button BtProperties "Properties" pos:[16,16] width:78 height:33
	button BtCreate "Create" pos:[103,16] width:86 height:33
	button BtProcess "Process" pos:[198,16] width:96 height:33
	button BtExport "Export" pos:[303,16] width:96 height:33
	
	on BtProperties pressed do
	(
		-- Basis UI
		UiFiles = #("Ui/PBXProjectPropertiesUi.ms","Ui/PBXWolrdPropertiesUi.ms")
		
		-- Load Properties Ui From Modules
		join UiFiles (GetUiFilesFromModules "#PropertiesUi#")
		
		-- Update Rollout
		RefreshPBXToolsRF UiFiles PBXToolsRF
	)
	
	on BtCreate pressed do
	(
		-- Basis UI
		UiFiles = #("Ui/PBXObjectInstanceUi.ms","Ui/PBXStaticObjectUi.ms","Ui/PBXLineObjectUi.ms","Ui/PBXCaptureObjectUi.ms","Ui/PBXArrayObjectUi.ms")
		join UiFiles (GetUiFilesFromModules "#CreateUi#")
		
		-- Update Rollout
		RefreshPBXToolsRF UiFiles PBXToolsRF
	)
	
	on BtProcess pressed do
	(
		-- Basis UI
		UiFiles = #("Ui/PBXMultiSetUi.ms")
		
		-- Load Properties Ui From Modules
		join UiFiles (GetUiFilesFromModules "#ProcessUi#")
		
		-- Update Rollout
		RefreshPBXToolsRF UiFiles PBXToolsRF
	)
	
	on BtExport pressed do
	(
		-- Basis UI
		UiFiles = #()
		
		-- Load Ui From Modules
		join UiFiles (GetUiFilesFromModules "#ExportUi#")
		
		-- Update Rollout
		RefreshPBXToolsRF UiFiles PBXToolsRF
	)
)