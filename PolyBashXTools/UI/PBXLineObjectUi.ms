
rollout PBXLineObjectStrUi "Line Object"
(

local LinInstanceRoot = undefined
	
	button BtMakeLine "Make Line Object"

	on BtMakeLine pressed do
	(
		LineObject = PBXLineObjectStr ()
		
		if selection.count > 0 do 
		(
			if selection.count > 1 do
			(
				for o in selection do LineObject.MakeDummy o
			)
			
			if selection.count == 1 do LineObject.MakeDummy $
		)
	)
	
)
