
rollout PBXStaticObjectUi "Static Object"
(
	local InstanceRoot = undefined
	
	button BtMakeStatic "Make Static Object"
	button BtMakeStaticSub "Make Static Sub Object"

	on BtMakeStatic pressed do
	(
		StaticObject = PBXStaticObjectStr ()
		
		if selection.count > 0 do 
		(
			if selection.count > 1 do
			(
				for o in selection do StaticObject.MakeDummy o
			)
			if selection.count == 1 do StaticObject.MakeDummy $
		)
	)
	
	on BtMakeStaticSub pressed do
	(
		StaticObject = PBXStaticObjectStr ()
		
		if selection.count > 0 do 
		(
			if selection.count > 1 do
			(
				for o in selection do StaticObject.MakeSubDummy o
			)
			if selection.count == 1 do StaticObject.MakeSubDummy $
		)
	)
)
