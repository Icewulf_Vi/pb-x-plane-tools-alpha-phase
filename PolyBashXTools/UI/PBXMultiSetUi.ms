
Fn PBXMultiSetUiGetListItems flag OnlySelected:False=
(
	tmp = #()
	if flag == "Static" do
	(
		for h in $Helpers do
		(
			if PBXStaticObjectStr.Is h do append tmp h
		)
	)
	
	if flag == "Line" do
	(
		for h in $Helpers do
		(
			if PBXLineObjectStr.Is h do append tmp h
		)
	)
	
	if flag == "Capture" do
	(
		for h in $Helpers do
		(
			if PBXCaptureStr.Is h do append tmp h
		)
	)
	
	if flag == "Array" do
	(
		for h in $Helpers do
		(
			if PBXArrayObjectStr.Is h do append tmp h
		)
	)

	if OnlySelected then
	(
		newTmp = #()
		for o in tmp do
		(
			id = findItem (selection as array) o
			if id != 0 do
			(
				append newtmp o
			)
		)
		tmp = newTmp
	)
	
	return tmp
)

Fn MakePropertyDialog flag =
(

	for i = 1 to PBXMultiSet.PropertiesRollout.rollouts.count-1 do
	(
		try 
		(
		removeSubRollout PBXMultiSetUi.PropertiesRollout PBXMultiSet.PropertiesRollout.rollouts[1]
		)
		catch
		()
	)
	
	if flag == "Static" do
	(
		tmpattr = (PBXNodeStr.GetMultiselectionAttributes "#StaticObjectMultiSetAttributes#")
	)
	
	if flag == "Line" do
	(
		tmpattr = (PBXNodeStr.GetMultiselectionAttributes "#LineObjectMultiSetAttributes#")
	)
	
	if flag == "Capture" do
	(
		tmpattr = (PBXNodeStr.GetMultiselectionAttributes "#CapturObjectMultiSetAttributes#")
	)
	
	if flag == "Array" do
	(
		tmpattr = (PBXNodeStr.GetMultiselectionAttributes "#ArrayObjectMultiSetAttributes#")
	)
	
	for o in tmpattr do
	(
		AddSubRollout PBXMultiSet.PropertiesRollout (filein (PBXToolsInstallationPath + o))
	)
)

Fn PBXMultiSetUiGetListItemNames items = 
(
	if items != undefined do
	(
		tmp = #()
		for o in items do append tmp o.Name
		return tmp
	)
	return #()
)

FileIn ((getFilenamePath (getThisScriptFilename())) + "PBXTextureMultiSetAttributes.ms")

rollout PBXMultiSet "Multi set" width:295 height:509
(
	local LbNodesItems = #()
	Local TypeFilterLabels = #("Static", "Line", "Capture", "Array")
	Local SelectedNodes = #()

	radioButtons TypeFilter "RadioButtons" pos:[10,12] width:66 height:78 labels:TypeFilterLabels default:5 columns:4 align:#left
	checkbox CbFilterOnlySelected "Only Selected" pos:[10,65] width:152 height:20 align:#left
	MultiListBox LbNodes "ListBox" pos:[10,100] width:215 height:25 align:#left
	subrollout PropertiesRollout "test1" align:#right pos:[240,120] width:200 height:320
	
	on TypeFilter changed arg do
	(
		LbNodesItems =  PBXMultiSetUiGetListItems TypeFilterLabels[arg] OnlySelected:CbFilterOnlySelected.State
		LbNodes.items = PBXMultiSetUiGetListItemNames LbNodesItems
	)
	
	on CbFilterOnlySelected changed arg do
	(
		LbNodesItems =  PBXMultiSetUiGetListItems TypeFilterLabels[TypeFilter.state] OnlySelected:CbFilterOnlySelected.State
		LbNodes.items = PBXMultiSetUiGetListItemNames LbNodesItems
	)
	
	on LbNodes selectionEnd do
	(
		SelectedNodes = #()
		for o in LbNodes.selection do
		(
			append SelectedNodes LbNodesItems[o]
		)
		
		MakePropertyDialog TypeFilterLabels[TypeFilter.state]
	)
)
