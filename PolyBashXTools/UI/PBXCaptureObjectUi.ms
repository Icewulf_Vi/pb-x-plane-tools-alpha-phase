
rollout PBXArrayCaptureStrUi "Capture Object"
(
	button BtMakeCapture "Make Capture Object"

	on BtMakeCapture pressed do
	(
		if selection.count > 0 do 
		(
			if selection.count > 1 do
			(
				for o in selection do PBXCaptureStr.MakeDummy o
			)
			
			if selection.count == 1 do PBXCaptureStr.MakeDummy $
		)
	)
)
