
rollout PBXScenePropertiesUi "Project Properties"
(
	local PropClass = PBXPropStr
	
	local vPjname = (PropClass.getFileProp "ProjectName")
	local vPjVersion = (PropClass.getFileProp "ProjectVersion")

	
		
	-----------------------------------------------------------------------------------------------------------	
	----eingabe Felder f�r die Airportdaten
	editText PjName "Project Name" text:vPjname
	editText PjVersion "Project Version" text:vPjVersion
	------------------------------------------------------------------------------------------------------------


	on PjName changed arg do
	(
		PropClass.SetFileProp "ProjectName" arg

	)
	
	on PjVersion changed arg do
	(
		PropClass.SetFileProp "ProjectVersion" arg

	)

)