struct PBXObjectInstanceUiItem
(
	Image = #((PBXToolsInstallationPath +"GFX\\ImgBtNone.png"),undefined, 1,1,1,1,1 ),
	RootID = ""
)
	
Fn PBXNodeRootPropertyPickFilter arg =
(
	if (isProperty arg "ID") == True do return true
	if (isProperty arg "RootID") == True do return true
	return false
)

rollout PBXObjectInstanceUi "Object Instance"
(

	
	local Instances = #(PBXObjectInstanceUiItem(),PBXObjectInstanceUiItem(),PBXObjectInstanceUiItem(),PBXObjectInstanceUiItem(),PBXObjectInstanceUiItem(),PBXObjectInstanceUiItem(),PBXObjectInstanceUiItem(),PBXObjectInstanceUiItem())
	local Selected = 1
		
	Button BtImage01 "None" images:Instances[1].Image Height:100 width:100 Pos:[10,10]
	Button BtImage02 "None" images:Instances[2].Image Height:100 width:100 Pos:[10,120]
		
	Button BtImage03 "None" images:Instances[3].Image Height:100 width:100 Pos:[120,10]
	Button BtImage04 "None" images:Instances[4].Image Height:100 width:100 Pos:[120,120]
		
	Button BtImage05 "None" images:Instances[5].Image Height:100 width:100 Pos:[230,10]
	Button BtImage06 "None" images:Instances[6].Image Height:100 width:100 Pos:[230,120]
		
	Button BtImage07 "None" images:Instances[7].Image Height:100 width:100 Pos:[340,10]
	Button BtImage08 "None" images:Instances[8].Image Height:100 width:100 Pos:[340,120]
	
	on BtImage01 pressed do Selected = 1
	on BtImage02 pressed do Selected = 2
	on BtImage03 pressed do Selected = 3
	on BtImage04 pressed do Selected = 4
	on BtImage05 pressed do Selected = 5
	on BtImage06 pressed do Selected = 6
	on BtImage07 pressed do Selected = 7
	on BtImage08 pressed do Selected = 8
	
	
	pickButton PiBtRoot "Pick Root" filter:PBXNodeRootPropertyPickFilter
	on PiBtRoot picked arg do
	(
		btImage = (PBCapture.ObjectButtonImage arg (PBXToolsTMP + "GFX\\InstSonjTeImg" + Selected as string +".png"))
		
		InstanceRoot = STOBJ
		
		tmp = PBXObjectInstanceUiItem ()

		tmp.RootID = arg.ID
		if arg.RootID == "" then
		(
			tmp.RootID = arg.ID
			tmp.Image = #(btImage, undefined, 1,1,1,1,1 )
		)
		else
		(
			tmp.RootID = arg.RootID
			tmp.Image = #(btImage, undefined, 1,1,1,1,1 )
		)
		
		Instances[Selected] = tmp
		
		-- update UI
		if Selected == 1 do BtImage01.images = Instances[1].Image
		if Selected == 2 do BtImage02.images = Instances[2].Image
		if Selected == 3 do BtImage03.images = Instances[3].Image
		if Selected == 4 do BtImage04.images = Instances[4].Image
		if Selected == 5 do BtImage05.images = Instances[5].Image
			
		if Selected == 6 do BtImage06.images = Instances[6].Image
		if Selected == 7 do BtImage07.images = Instances[7].Image
		if Selected == 8 do BtImage08.images = Instances[8].Image
	)
	
	button BTMakeToInstance "Replace Selection with Instance"
	on BTMakeToInstance pressed do
	(
		print Instances[Selected].RootID
		if Instances[Selected].RootID != "" do
		(
			InstanceObject = PBXInstanceObjectStr RootID:Instances[Selected].RootID
			if selection.count > 0 do 
			(
				if selection.count > 1 do
				(
					for o in selection do InstanceObject.Convert o
				)
				
				if selection.count == 1 do InstanceObject.Convert $
			)
		)
	)
	
	button BTMakeSetInstance "Set instance here"
	on BTMakeSetInstance pressed do
	(
		print Instances[Selected].RootID
		if Instances[Selected].RootID != "" do
		(
			InstanceObject = PBXInstanceObjectStr RootID:Instances[Selected].RootID
			if selection.count > 0 do 
			(
				if selection.count > 1 do
				(
					for o in selection do InstanceObject.MakeDummy o
				)
				
				if selection.count == 1 do InstanceObject.MakeDummy $
			)
		)
	)

		
	--checkButton BTClickPlaceInstance "Click Point Instance"
	---------
	
	

	


	
	on PiBtRootStatic picked STOBJ do
	(
		btImage = (PBCapture.ObjectButtonImage STOBJ (PBXToolsTMP + "GFX\\InstSonjTeImg.png"))
		BtPiImage.images = #(btImage, undefined, 1,1,1,1,1 )
		
		InstanceRoot = STOBJ
	)
	
	on BTClickPlaceInstance changed Status do
	(
		if Status == True then
		(
			PBClickPointNodes = #()
			startTool PBClickPoints
		)
		else
		(
			StaticObjectInstance = PBXSaticObjectInstance InstanceRoot:InstanceRoot
			StaticObjectInstance.TranslateToInstance PBClickPointNodes
			
			for N in PBClickPointNodes do if isvalidnode n == true do delete n
		)
	)
)
