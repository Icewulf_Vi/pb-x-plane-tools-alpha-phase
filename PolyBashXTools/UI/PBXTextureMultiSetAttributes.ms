


	Rollout PBXTextureMultiSetTextureSize "Texture size" height:150
	(
		spinner SpWidth "TextureWidth" range:[2,4096,0] scale:2 setKeyBrackets:False type:#Integer
		spinner SpHeight "TextureHeight" range:[2,4096,0] scale:2 setKeyBrackets:False type:#Integer
		
		on SpWidth changed arg do
		(
			for o in PBXMultiSet.SelectedNodes do
			(
				PBXNodeTexture.SetSize o (point2 SpWidth.value SpHeight.value) Id:MatID
			)
		)
		
		on SpHeight changed arg do
		(
			for o in PBXMultiSet.SelectedNodes do
			(
				PBXNodeTexture.SetSize o (point2 SpWidth.value SpHeight.value) Id:MatID
			)

		)
		
	)