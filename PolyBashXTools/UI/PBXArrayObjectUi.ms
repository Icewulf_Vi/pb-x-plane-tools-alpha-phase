
rollout PBXArrayObjectStrUi "Array Object"
(
	button BtMakeArray "Make Array Object"

	on BtMakeArray pressed do
	(
		PBXArrayObjectStr.MakeDummy ()
	)
	
	
	button BtTranslate "Translate to Array Object"

	on BtTranslate pressed do
	(
		select (PBXArrayObjectStr.Translate selection)
	)
	
	button BTCapture "Capture to Array Object"

	on BtCapture pressed do
	(
		select (PBXArrayObjectStr.Translate selection DeleteInstances:False)
	)
)
