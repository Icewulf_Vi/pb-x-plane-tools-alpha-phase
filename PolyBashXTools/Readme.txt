PBX Tools v0.0.2.3

How to configure:
Open PBXMAXHook.ms with a texteditor and change the property toolsStartPath to the correct filepath.
Use \\ for \. Don�t copy the files into some UAC protected folder like Programms\Autodesk.....

Set ...PolyBashXTools\Modules\Xplane\XplaneOBJ.dll as trusted.
Set ...PolyBashXTools\Structs\PBGeoCalc.dll as trusted.

Start 3DS Max and run the script PBXMAXHook.ms

If the error "Can�t start tool, startup file not found!" appears you are doomed or your path is not correct.
Else PBX Tools will be ready to be added to your Ui

You need the latest .Net Framework to run all tools.