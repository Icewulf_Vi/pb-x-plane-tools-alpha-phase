/*
3DMaxHook used to connect PBXTools with 3DS Max
*/

macroScript StatPBXTools
	category:"PolyBash" 
	toolTip:"PBX Tools"
	buttonText:"PBX Tools"
	(
		-- Change this filepath to you installation directory of the tools.
		Local toolsStartPath = "D:\\-Projects-\\-Max Script-\\PB XTools\\Scripts\\PolyBashXTools\\StartPBXTools.ms"

		if (doesfileexist toolsStartPath) then
		(
			fileIn (toolsStartPath)
		)
		else
		(
			messagebox ("Can´t start tool, startup file not found!")
		)
	)

