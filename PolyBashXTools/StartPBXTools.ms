gc()

fn CryptedOrNot file =
(
	if doesfileexist file then Filein file 
		else
			if doesfileexist (f = ((getFilenamePath file) + (GetFilenameFile file)+ ".mse")) do
				Filein f
)

-- This must be loaded in 3DS Max Staurtup
-- This Starts The Tools and Load Classes

-- Starting PBX Tools	
--Set Global Vars
Global PBXToolsInstallationPath = (getFilenamePath (getThisScriptFilename())) -- Main Istallation Path
Global PBXToolsTMP = (getFilenamePath (getThisScriptFilename()) + "TMP\\")
print "#######"
print "Installationpath:"
print PBXToolsInstallationPath
Print "TMPPath:"
print PBXToolsTMP



print "#######"
print "Debug messages are stored in Global var <PBXNODEDebug>"
global PBXNODEDebug = ""
fn Debug inString inData Argument1:undefined Argument2:undefined Argument3:undefined=
(
	tmp = "\n" + inString + "\nArgument1: " + inData as string
	if Argument1 != undefined do tmp += "\nArgument1: " + Argument1 as string
	if Argument2 != undefined do tmp += "\nArgument2: " + Argument2 as string
	if Argument3 != undefined do tmp += "\nArgument3: " + Argument3 as string
	
	--print tmp
	if PBXNODEDebug.count > 1000 do PBXNODEDebug = ""
	PBXNODEDebug += tmp
)

Global PBClickPointNodes = #() -- Store Information from ClickTools

-- Load Classes
print "#######"
print "LoadData"

CryptedOrNot (PBXToolsInstallationPath + "Structs\\PBMaterialAndTexture.ms")
	
CryptedOrNot (PBXToolsInstallationPath + "Structs\\PolyBashGeocoordStr.ms")
	
CryptedOrNot (PBXToolsInstallationPath + "Structs\\PolyBashUtilitysStr.ms")

CryptedOrNot (PBXToolsInstallationPath + "Structs\\PolyBashXPropStr.ms")

CryptedOrNot (PBXToolsInstallationPath + "Structs\\PBXMapQueue.ms")

CryptedOrNot (PBXToolsInstallationPath + "Structs\\Node\\PolyxBashX_StaticObject_Helper.ms")

CryptedOrNot (PBXToolsInstallationPath + "Structs\\Node\\PolyxBashX_StaticSubObject_Helper.ms")

CryptedOrNot (PBXToolsInstallationPath + "Structs\\Node\\PolyxBashX_Array_Helper.ms")

CryptedOrNot (PBXToolsInstallationPath + "Structs\\Node\\PolyxBashX_LineObject_Helper.ms")

CryptedOrNot (PBXToolsInstallationPath + "Structs\\Node\\PolyxBashX_Capture_Helper.ms")

CryptedOrNot (PBXToolsInstallationPath + "Structs\\Node\\PolyxBashX_InstanceObject_Helper.ms")
	
CryptedOrNot (PBXToolsInstallationPath + "Structs\\Node\\PolyBashXNodeStr.ms")

CryptedOrNot (PBXToolsInstallationPath + "Structs\\Node\\PolyBashXNodeStaticObjectStr.ms")

CryptedOrNot (PBXToolsInstallationPath + "Structs\\Node\\PolyBashXNodeLineObjectStr.ms")

CryptedOrNot (PBXToolsInstallationPath + "Structs\\Node\\PolyBashXNodeCaptureObjectStr.ms")

CryptedOrNot (PBXToolsInstallationPath + "Structs\\Node\\PolyBashXNodeArrayObjectStr.ms")

CryptedOrNot (PBXToolsInstallationPath + "Structs\\Node\\PolyBashXNodeTexture.ms")


print "#######"
print "Prepare Ui"
Global PBXToolsRF -- Stores the UI
--Start Ui
Try closeRolloutFloater PBXToolsRF catch ()
PBXToolsRF = NewRolloutFloater "X-Plane Tools" 470 460 
-- Load Main UI
addRollout (fileIn (PBXToolsInstallationPath + "Ui\\PBXMainUi.ms")) PBXToolsRF		

print "#######"
print "Done"