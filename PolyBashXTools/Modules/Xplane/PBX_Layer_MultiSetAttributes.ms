	Rollout Params "X Plane Layer Properties" height:150
	(
		local XPSOBJPropClass = PBXPropStr CFGFilePath:(PBXToolsInstallationPath + "\\Modules\\Xplane\\Xplane.cfg")
		
		----------- LAYER ---------------
		Checkbox UseLayer "use Layer" Tristate:2
		spinner Layer "Layer" width:50  range:[-5,5,0] scale:1 type: #integer
		dropdownlist modLayergroup "Layergroup" items:(XPSOBJPropClass.getfromcfg "#LayerGroups#" True)
		
		on UseLayer changed val do
		(	
			for o in PBXMultiSet.SelectedNodes do
			(
				o.UseLayer = val
			)
		)
		
		on Layer changed val do
		(	
			for o in PBXMultiSet.SelectedNodes do
			(
				o.Layer = val
			)
		)
		
		on modLayergroup selected arg do
		(
			for o in PBXMultiSet.SelectedNodes do
			(
				o.Layergroup = modLayergroup.items[arg]
			)
			
		)
		-----------
	)