setCommandPanelTaskMode #create
-- Basic Setup
PBXLine_TypeCode = "PBXLIN"
PBXCapture_TypeCode = "PBXCAP"

--Load Classes
FileIn ((getFilenamePath (getThisScriptFilename())) + "PBX_XPlaneFileFormats.ms")
PathingClass = PBFilePathing ()
PropClass = PBXPropStr ()
MattClass =	PBMaterialAndTexture()
GeoClass = PBGeoCordStr \
	WorldZeroCordLat:(PropClass.getFileProp "WorldZeroCordLat") \
	WorldZeroCordLon:(PropClass.getFileProp "WorldZeroCordLon") \
	WorldZeroCordAlt:(PropClass.getFileProp "WorldZeroCordAlt") 
	
-- SetVars
NodesToExport = #()
ExportPath = (PropClass.getFileProp "XPlaneExportPath")

TMPStdSaveSubPath = "ExportTMP\\"
POLStdSaveSubPath = "Resources\\Objects\\"
CAPStdSaveSubPath = "Resources\\Objects\\"

Print "Start export POL to Xplane"

Rollout PBXXplanePolExportStatusPrompt "PBXXplanePolExport"
(
	Label Lb "Writing Pol data"
	Progressbar PbStatus value:0.0
)

TmpPolLineObjects = PBXNodeStr.GetByTypeCode PBXLine_TypeCode
TmpPolCapturObjects = (PBXNodeStr.GetByTypeCode PBXCapture_TypeCode)
NodeCount = (TmpPolLineObjects.count + TmpPolCapturObjects.count)
CurrentNode = 0

createdialog PBXXplanePolExportStatusPrompt
CurrentNode +=1 
PBXXplanePolExportStatusPrompt.PbStatus.value = ((CurrentNode as float)/(NodeCount as float) * 100)
	

--###############################
-- Lines
--##################################################
for o in TmpPolLineObjects do
(
	CurrentNode +=1 
	PBXXplanePolExportStatusPrompt.PbStatus.value = ((CurrentNode as float)/(NodeCount as float) * 100)
	
	if o.WritePol and (PBXLineObjectStr.HasFill o) do
	(
		print ("Export:" + o.name)
		try
		(		
			-- Get Texture Informations
			MaxMaterialName = (PBXNodeTexture.GetMap o "MaterialName" Id:2)
			if MaxMaterialName != undefined do
			(
				MaxDiffMap = (PBXNodeTexture.GetQueueMap o "Diffuse" Id:2)
				MaxOpacMap = (PBXNodeTexture.GetQueueMap o "Opacity" Id:2)
				MaxSpecMap = (PBXNodeTexture.GetQueueMap o "Specular" Id:2)
				
			
				if MaxDiffMap != undefined or MaxOpacMap != undefined do
				(
					Local DiffTextureName = (ExportPath + TMPStdSaveSubPath + MaxMaterialName +"_Diff.png")
					Rasterizer = #(MaxDiffMap,MaxOpacMap,(PBXNodeTexture.getSize o),DiffTextureName)
				--	print Rasterizer
					append PBXMapQueue.PBXQuedMapRenderBitmap Rasterizer
					
					QueCode = "PBXTempA = \"" + toupper (DiffTextureName as string) + " \"" + " \n"
					QueCode +="PBXTempB = \"" + ((toupper ExportPath) + (toupper OBJStdSaveSubPath)) as string + " \"" + " \n"
					QueCode += "PBXXplaneDataCoverter.ConvertToXplaneDDS PBXTempA PBXTempB"

					append PBXMapQueue.PBXMapQueScriptCode (ScriptString.Convert QueCode)
					DiffuseTexture = (ExportPath + OBJStdSaveSubPath + MaxMaterialName +"_Diff.dds")
				)
				
				if MaxSeIlMap != undefined do
				(
					Local LitTextureName = (ExportPath + TMPStdSaveSubPath + MaxMaterialName +"_Lit.png")
					Rasterizer = #(MaxSeIlMap,undefined,(PBXNodeTexture.getSize o),LitTextureName)
					--print Rasterizer
					append PBXMapQueue.PBXQuedMapRenderBitmap Rasterizer
					
					QueCode = "PBXTempA = \"" + toupper (LitTextureName as string) + " \"" + " \n"
					QueCode +="PBXTempB = \"" + ((toupper ExportPath) + (toupper OBJStdSaveSubPath)) as string + " \"" + " \n"
					QueCode += "PBXXplaneDataCoverter.ConvertToXplaneDDS PBXTempA PBXTempB"

					append PBXMapQueue.PBXMapQueScriptCode (ScriptString.Convert QueCode)
					LitTexture = (ExportPath + OBJStdSaveSubPath + MaxMaterialName +"_Lit.dds")
				)
			)
			
			for LineItem in (PBXLineObjectStr.GetItems o) do
			(
				if LineItem.FillEdge do
				(
					TheDrapedPolygone = PBXXPlanePOL \
					FileName: (PBXNodeStr.GetExportName o) \
					FileSavePath:(ExportPath + POLStdSaveSubPath) \
					DiffuseTexture:DiffuseTexture \
					LitTexture:LitTexture \
					TextureNoWrap:False \
					WorldScale: LineItem.UVScale \
					UseLayer: o.UseLayer \
					Layer:o.Layer \
					LayerGroup: o.Layergroup
				--	SurfaceType:
				--	noAlpha:
				--	LoadCenter:
					
					TheDrapedPolygone.WritePOL ()
				)
			)
		)
		catch
		(
			print ("Error:Exporting " + o.name + "failed.")
		)
	)
)
--##################################################


--## Captures
for o in TmpPolCapturObjects do
(
	CurrentNode +=1 
	PBXXplanePolExportStatusPrompt.PbStatus.value = ((CurrentNode as float)/(NodeCount as float) * 100)
	
	if o.WriteAsPol do
	(
		print ("Export:" + o.name)
		try
		(
			if o.CaptureType == (PBXCaptureStr.GetCaptureTypes())[1] do
			(
				MaxMaterialName = PBXNodeStr.GetExportName o
				if MaxMaterialName != undefined do
				(
					MaxDiffMap = (PBXNodeTexture.GetMap o "Diffuse")
					MaxOpacMap = (PBXNodeTexture.GetMap o "Opacity")
					MaxSeIlMap = (PBXNodeTexture.GetMap o "Selfillum")
					
					OutTextureRes = (PBXNodeTexture.getSize o)
					DiffuseMapName = "NotAssigned"
					if MaxDiffMap != undefined or MaxOpacMap != undefined do
					(
						DiffTex = MattClass.RenderBitmap \
						MaxDiffMap \
						MaxOpacMap \
						OutTextureRes \
						(ExportPath + TMPStdSaveSubPath + MaxMaterialName +"_Diff.png")
						
						-- Convert Texture to dds and add it to the ExportObj
						DiffuseMapName = PBXXplaneDataCoverter.ConvertToXplaneDDS DiffTex (ExportPath + CAPStdSaveSubPath)
					)
					
					if MaxSeIlMap != undefined do
					(
						LitTex = MattClass.RenderBitmap \
						MaxSeIlMap \
						undefined \
						OutTextureRes \
						(ExportPath + TMPStdSaveSubPath + MaxMaterialName +"_Lit.png")
						
						LitMapName = PBXXplaneDataCoverter.ConvertToXplaneDDS LitTex (ExportPath + CAPStdSaveSubPath)
					)
				)
				
				TheDrapedPolygone = PBXXPlanePOL \
				FileName: (PBXNodeStr.GetExportName o) \
				FileSavePath:(ExportPath + CAPStdSaveSubPath) \
				DiffuseTexture:DiffusemapName \
				LitTexture:LitMapName \
				WorldScale: 1 \
				UseLayer: o.UseLayer \
				Layer:o.Layer \
				LayerGroup: o.Layergroup \
				LoadCenter:true \
				LoadCenterPos:(GeoClass.GetGeoPos o.pos) \
				LoadCenterDim:(o.Max-o.min).Y \
				LoadCenterTextureSize:(PBXNodeTexture.getSize o).Y \
				TextureNoWrap:true
				
				TheDrapedPolygone.WritePOL ()
			)
		)
		catch
		(
			print ("Error:Exporting " + o.name + "failed.")
		)
	)
)

destroydialog PBXXplanePolExportStatusPrompt