PBX_SOBJNode_CustAttributes = attributes "PBXLIN_Xplane_CustAtt"
(
	Parameters main rollout:params
	(
		WriteLin Type:#Boolean Ui:CbWriteLin default:true
		WritePol Type:#Boolean Ui:CbWritePol default:true
		WriteDSFPlacement Type:#Boolean Ui:CbWriteDSFPlacement default:true
		WriteStr Type:#Boolean Ui:CbWriteLin default:false
	)
	
	Rollout Params "X Plane Properties"
	(
		CheckBox CbWriteDSFPlacement "Write in DSF"
		
		CheckBox CbWriteLin "Write Lin"
		CheckBox CbWritePol "Write Pol"
	--	CheckBox CbWriteStr "Write Str"

	)
)