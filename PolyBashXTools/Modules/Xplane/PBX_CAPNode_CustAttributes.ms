PBX_CAPNode_CustAttributes = attributes "PBXCAP_Xplane_CustAtt"
(
	Parameters main rollout:params
	(
		WriteAsPol Type:#Boolean Ui:CbWriteAsPol default:true
		WriteAsObj Type:#Boolean Ui:CbWriteAsObj default:false
		WriteDSFPlacement Type:#Boolean Ui:CbWriteDSFPlacement default:true
	)
	
	Rollout Params "X Plane Properties"
	(
		CheckBox CbWriteDSFPlacement "Write in DSF"
		
		CheckBox CbWriteAsPol "Write as POL"
		
		CheckBox CbWriteAsObj "Write as OBJ"
		
		on CbWriteAsObj changed arg do CbWriteAsPol.checked = not arg
		on CbWriteAsPol changed arg do CbWriteAsObj.checked = not arg
	)
)