PBX_SOBJNode_CustAttributes = attributes "PBXSOBJ_XplaneLOD_CustAtt"
(
Parameters main rollout:params
	(
		UseLod Type:#Boolean ui:CbUselod
		LowLod Type:#Float ui:CbLowlod
		HighLod Type:#Float ui:CbHighlod
	)
	
	Rollout Params "X Plane LOD Properties"
	(

		----------- LOD ---------------
		checkbox CbUseLod "Use LOD" Tristate:2
		spinner Cblowlod "Low LOD"  width:100  range:[0,100000,0] scale:0.5 enabled:(UseLod) Type:#worldunits
		spinner CbHighLod "High LOD" width:100 range:[0,100000,500] scale:0.5 enabled:(UseLod) Type:#worldunits
		
		on CbUselod changed arg do 
		(
			Cblowlod.enabled = arg
			CbHighLod.enabled = arg
		)
		----------------------------------
		
		on CbUseLod changed arg do
		(
			CbUseLod.enabled = arg
			Cblowlod.enabled = arg
			CbHighLod.enabled = arg

		)

	)
)