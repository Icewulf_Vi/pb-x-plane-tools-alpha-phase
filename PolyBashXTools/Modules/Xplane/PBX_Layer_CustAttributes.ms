PBX_SOBJNode_CustAttributes = attributes "PBXSOBJ_XplaneLayer_CustAtt"
(
	Parameters main rollout:params
	(
		UseLayer Type:#Boolean ui:UseLayer
		LayerGroup Type:#String
		Layer Type:#Integer Ui:Layer

	)
	
	Rollout Params "X Plane Layer Properties"
	(
		local XPSOBJPropClass = PBXPropStr CFGFilePath:(PBXToolsInstallationPath + "\\Modules\\Xplane\\Xplane.cfg")
		
		----------- LAYER ---------------
		Checkbox UseLayer "use Layer" Tristate:2 state:($.UseLayer)
		spinner Layer "Layer" width:50  range:[-5,5,0] scale:1 enabled:($.UseLayer) type: #integer
		dropdownlist modLayergroup "Layergroup" items:(XPSOBJPropClass.getfromcfg "#LayerGroups#" True) enabled:($.UseLayer) selection:(findItem (modLayergroup.Items as array) $.Layergroup)
		
		on UseLayer changed val do
		(	
			Layer.enabled = val
			modLayergroup.enabled = val
		)
		
		on modLayergroup selected arg do
		(
			$.Layergroup = modLayergroup.items[arg]
		)
		-----------
	)
)