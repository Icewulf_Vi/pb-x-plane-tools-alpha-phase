setCommandPanelTaskMode #create
-- Basic Setup
PBXArray_TypeCode = "PBXARR"

--Load Classes
FileIn ((getFilenamePath (getThisScriptFilename())) + "PBX_XPlaneFileFormats.ms")
PathingClass = PBFilePathing ()
PropClass = PBXPropStr ()
MattClass =	PBMaterialAndTexture()
GeoClass = PBGeoCordStr \
	WorldZeroCordLat:(PropClass.getFileProp "WorldZeroCordLat") \
	WorldZeroCordLon:(PropClass.getFileProp "WorldZeroCordLon") \
	WorldZeroCordAlt:(PropClass.getFileProp "WorldZeroCordAlt") 
	
-- SetVars
NodesToExport = #()
ExportPath = (PropClass.getFileProp "XPlaneExportPath")

TMPStdSaveSubPath = "ExportTMP\\"
OBJStdSaveSubPath = "Resources\\Objects\\"
AGNStdSaveSubPath = "Resources\\Objects\\" 
DSFStdSaveSubPath = "Earth nav data\\"
LINStdSaveSubPath = "Resources\\Objects\\"


Print "Start export AGP to Xplane"

--#################################
-- Autogen Points
--##################################################

TMPXplaneAGPObjects = (PBXNodeStr.GetByTypeCode PBXArray_TypeCode)

Rollout PBXXplaneAPGExportStatusPrompt "PBXXplaneLinExport"
(
	Label Lb "Writing AGP data"
	Progressbar PbStatus value:0.0
)

--status variables
NodeCount = TMPXplaneAGPObjects.count
CurrentNode = 0





createdialog PBXXplaneAPGExportStatusPrompt


For o in TMPXplaneAGPObjects do
(
	-- Update status
	CurrentNode +=1 
	PBXXplaneAPGExportStatusPrompt.PbStatus.value = ((CurrentNode as float)/(NodeCount as float) * 100)
	
	-- Export AGN
	ExpAGP = PBXXPlaneAGP \
	FileName:(PBXNodeStr.GetExportName o) \
	FileSavePath:(ExportPath + AGNStdSaveSubPath) \
	AGPObjectDefs:#() \
	AGPObjectPlacements:#()  \
	AGPTextureXY: (point2 (o.XDim as integer) (o.yDim as integer)) \ 
	WorldSize: (point2 o.XDim o.yDim)  \
	Texture = "NoTexture"
	
	ArrayItems =  PBXArrayObjectStr.GetSortedByID o
	print ArrayItems.count

	for AI in ArrayItems do
	(
		ExpAGP.AddToAGPObjectDefs OBJFileName:AI.NodeID
		print AI

		for i = 1 to AI.Positions.count do
		(
			print (Ai.Rotations[i] as string + " " + Ai.Positions[i] as string)
			tmprot = Ai.Rotations[i] as EulerAngles
			
			ExpAGP.AddToAGPObjectPlacements X:Ai.Positions[i].x Y:Ai.Positions[i].y Heading:tmprot.Z_Rotation
		)
		
		ExpAGP.AGPIndexID+= 1
	)
	ExpAGP.WriteAGP ()
)
--##################################################

destroydialog PBXXplaneAPGExportStatusPrompt


