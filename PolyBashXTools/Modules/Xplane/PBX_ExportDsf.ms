setCommandPanelTaskMode #create
-- Basic Setup
PBXStaticObject_TypeCode = "PBXSOBJ"
PBXArray_TypeCode = "PBXARR"
PBXLine_TypeCode = "PBXLIN"
PBXCapture_TypeCode = "PBXCAP"

--Load Classes
FileIn ((getFilenamePath (getThisScriptFilename())) + "PBX_XPlaneFileFormats.ms")
PathingClass = PBFilePathing ()
PropClass = PBXPropStr ()
MattClass =	PBMaterialAndTexture()
GeoClass = PBGeoCordStr \
	WorldZeroCordLat:(PropClass.getFileProp "WorldZeroCordLat") \
	WorldZeroCordLon:(PropClass.getFileProp "WorldZeroCordLon") \
	WorldZeroCordAlt:(PropClass.getFileProp "WorldZeroCordAlt") 
	
-- SetVars
NodesToExport = #()
ExportPath = (PropClass.getFileProp "XPlaneExportPath")

TMPStdSaveSubPath = "ExportTMP\\"
OBJStdSaveSubPath = "Resources\\Objects\\"
AGNStdSaveSubPath = "Resources\\Objects\\" 
DSFStdSaveSubPath = "Earth nav data\\"
LINStdSaveSubPath = "Resources\\Objects\\"
POLStdSaveSubPath = "Resources\\Objects\\"
CAPStdSaveSubPath = "Resources\\Objects\\"


Print "Start DSF To Xplane"



Rollout PBXXplaneDSFExportStatusPrompt "PBXXplaneDSFExport"
(
	Label Lb "Writing DFS data"
	Progressbar PbStatus value:0.0
)

TmpStaticObjectNodes = (PBXNodeStr.GetByTypeCode PBXStaticObject_TypeCode)
TmpLinObjectNodes = (PBXNodeStr.GetByTypeCode PBXLine_TypeCode)
TmpCaptureObjectNodes = (PBXNodeStr.GetByTypeCode PBXCapture_TypeCode)
TmpAgpObjectNodes = (PBXNodeStr.GetByTypeCode PBXArray_TypeCode)

NodeCount = (TmpStaticObjectNodes.count + TmpLinObjectNodes.count + TmpCaptureObjectNodes.count + TmpAgpObjectNodes.count)
CurrentNode = 0

createdialog PBXXplaneDSFExportStatusPrompt


-- Make DSF File
ExpDSF = PBXXPlaneDSF \
FileSavePath:(ExportPath + TMPStdSaveSubPath) \
DSFLat:(GeoClass.WorldZeroCordLat) \
DSFLon:(GeoClass.WorldZeroCordLon) \
DSFAlt:(GeoClass.WorldZeroCordAlt) \
DSFObjectDefs:#() \
DSFObjectPlacements:#()


--Object
For o in TmpStaticObjectNodes do
(
	
	CurrentNode +=1 
	PBXXplaneDSFExportStatusPrompt.PbStatus.value = ((CurrentNode as float)/(NodeCount as float) * 100)
	
	if o.WriteDSFPlacement do
	(
		numSubs = PBXNodeTexture.GetNumSubs o
		for i = 1 to numSubs do
		(
			if NumSubs == 1 then
			(
				ID = Undefined
			)
			else
			(
				ID = i
			)
			-- Write Object into DSF

			-- Add OBJdefination To DSF File
			ExpDSF.AddObjectDef OBJFileName:(PBXNodeStr.GetExportName o ID:ID) ObjSubpath:OBJStdSaveSubPath
			
			-- Add OBPlacement To DSF File
			ExpDSF.AddObjectPlacement GeoCordinates:(GeoClass.GetGeoPos o.pos) Heading:(o.rotation.Z_Rotation) SitOnGround:o.SitOnGround
			
			-- Write Instances into DSF
			instaces = (PBXNodeStr.GetByRootID o.ID)
			if instaces.count > 0 do
			(
				-- Add OBJdefination To DSF File if needed
				if o.WriteDSFPlacement == false do ExpDSF.AddObjectDef OBJFileName:(PBXNodeStr.GetExportName o ID:ID) ObjSubpath:OBJStdSaveSubPath
				-- Loop Instances
				for n in instaces do
				(
					ExpDSF.AddObjectPlacement GeoCordinates:(GeoClass.GetGeoPos n.pos) Heading:(n.rotation.Z_Rotation) SitOnGround:o.SitOnGround
				)
			)
		)
	)

)


For o in TmpAgpObjectNodes do
(
	CurrentNode +=1 
	PBXXplaneDSFExportStatusPrompt.PbStatus.value = ((CurrentNode as float)/(NodeCount as float) * 100)
	
	ExpDSF.AddAutogenPointDef AGPFileName:(PBXNodeStr.GetExportName o) AGPSubpath:OBJStdSaveSubPath
	
	ExpDSF.AddAutogenPointPlacement GeoCordinates:(GeoClass.GetGeoPos o.pos) Heading:(o.rotation.Z_Rotation) SitOnGround:True
	
	for n in (PBXNodeStr.GetByRootID o.ID) do
	(
		ExpDSF.AddAutogenPointPlacement GeoCordinates:(GeoClass.GetGeoPos n.pos) Heading:(n.rotation.Z_Rotation) SitOnGround:True
	)
)

-- Line
Struct KnotData
(
	lat = undefined,
	lon = undefined,
	Alt = undefined,
	iVLat = undefined,
	iVLon = undefined,
	iVAlt = undefined,
	oVLat = undefined,
	oVLon = undefined,
	oVAlt = undefined,
	UV = undefined,
	Type = undefined
)



for o in TmpLinObjectNodes do
(
	
	CurrentNode +=1 
	PBXXplaneDSFExportStatusPrompt.PbStatus.value = ((CurrentNode as float)/(NodeCount as float) * 100)
	
	if o.WriteDSFPlacement do
	(
		Fn TranslatePBXShapePointsToXplane PBPointArray InstanceOffset:(point3 0 0 0) =
		(
-- add or every knot the type
			outdata = #()
			for p in PBPointArray do
			(
				tmp = KnotData ()
				pos = GeoClass.GetGeoPos (p.position + InstanceOffset)
				ivec = GeoClass.GetGeoPos p.invector
				ovec = GeoClass.GetGeoPos p.outvector
				
				tmp.lat = pos.lat
				tmp.lon = pos.lon
				tmp.Alt = pos.alt
				tmp.iVLat = ivec.lat
				tmp.iVLon = ivec.lon
				tmp.iVAlt = ivec.alt
				tmp.oVLat = ovec.lat
				tmp.oVLon = ovec.lon
				tmp.oVAlt = ovec.alt
				
				append outdata tmp 
			)
			return outdata
		)
			
		if PBXLineObjectStr.HasFill o do
		(
			if o.WriteDSFPlacement do
			(
				ExpDSF.AddPOLDef POLFilename:(PBXNodeStr.GetExportName o) PolSubPath:POLStdSaveSubPath
				
				TheShapes = PBXLineObjectStr.GetPoints o CWSWinding:false
				
				for s in TheShapes do
				(
					if S.Closed do
					(
						PointType = "Corner"
					
						if S.HasCorner == true do PointType = "Corner"
						if S.HasSmooth == true do PointType = "Smooth"
						if S.HasBezier == true do PointType = "Bezier"
-- Mapping rotation must be added
						ExpDSF.AddPOLPlacement Knots:(TranslatePBXShapePointsToXplane S.Points) PointType:PointType MappingRotation:0
					)
				)
			)
			
			instaces = (PBXNodeStr.GetByRootID o.ID)
			if instaces.count > 0 do
			(	
				if not o.WriteDSFPlacement do ExpDSF.AddPOLDef POLFilename:(PBXNodeStr.GetExportName o) PolSubPath:POLStdSaveSubPath
				
				for i in instaces do
				(
					TheShapes = PBXLineObjectStr.GetPoints i CWSWinding:false
				
					for s in TheShapes do
					(
						if S.Closed do
						(
							PointType = "Corner"
						
							if S.HasCorner == true do PointType = "Corner"
							if S.HasSmooth == true do PointType = "Smooth"
							if S.HasBezier == true do PointType = "Bezier"
-- Mapping rotation must be added
							ExpDSF.AddPOLPlacement Knots:(TranslatePBXShapePointsToXplane S.Points) PointType:PointType MappingRotation:0
						)
					)
				)
			)
			
		)

		if PBXLineObjectStr.HasEdgeOverlay o do
		(
			if o.WriteDSFPlacement do
			(
				ExpDSF.AddLineDef PolygoneFilename:(PBXNodeStr.GetExportName o) LineSubPath:LINStdSaveSubPath
				
				TheShapes = PBXLineObjectStr.GetPoints o
				
				for s in TheShapes do
				(
					PointType = "Corner"
					
					if S.HasCorner == true do PointType = "Corner"
					if S.HasSmooth == true do PointType = "Smooth"
					if S.HasBezier == true do PointType = "Bezier"
					ExpDSF.AddLinePlacement Knots:(TranslatePBXShapePointsToXplane S.Points) PointType:PointType closed:S.Closed
				)
			)
			
			-- Loop Instances
			instaces = (PBXNodeStr.GetByRootID o.ID)
			if instaces.count > 0 do
			(
				if not o.WriteDSFPlacement do ExpDSF.AddLineDef PolygoneFilename:(PBXNodeStr.GetExportName o) LineSubPath:LINStdSaveSubPath
				
				for i in instaces do
				(
					TheShapes = PBXLineObjectStr.GetPoints i
				
					for s in TheShapes do
					(
						PointType = "Corner"
						
						if S.HasCorner == true do PointType = "Corner"
						if S.HasSmooth == true do PointType = "Smooth"
						if S.HasBezier == true do PointType = "Bezier"
						ExpDSF.AddLinePlacement Knots:(TranslatePBXShapePointsToXplane S.Points) PointType:PointType closed:S.Closed
					)
				)
			)
		)

	)
)

--- Capture
for o in TmpCaptureObjectNodes do
(
	
	
	CurrentNode +=1 
	PBXXplaneDSFExportStatusPrompt.PbStatus.value = ((CurrentNode as float)/(NodeCount as float) * 100)
	
	if o.WriteAsOBJ and o.WriteDSFPlacement do
	(
		-- Add OBJdefination To DSF File
		ExpDSF.AddObjectDef OBJFileName:(PBXNodeStr.GetExportName o) ObjSubpath:POLStdSaveSubPath
		
		-- Add OBPlacement To DSF File
		ExpDSF.AddObjectPlacement GeoCordinates:(GeoClass.GetGeoPos o.pos) Heading:(o.rotation.Z_Rotation) SitOnGround:true
		-- Write Instances into DSF
		instaces = (PBXNodeStr.GetByRootID o.ID)
		if instaces.count > 0 do
		(
			-- Add OBJdefination To DSF File if needed
			if o.WriteDSFPlacement == false do ExpDSF.AddObjectDef OBJFileName:(PBXNodeStr.GetExportName o) ObjSubpath:POLStdSaveSubPath
			-- Loop Instances
			for n in instaces do
			(
				ExpDSF.AddObjectPlacement GeoCordinates:(GeoClass.GetGeoPos n.pos) Heading:(n.rotation.Z_Rotation) SitOnGround:true
			)
		)
	)
	
	if o.WriteAsPOL and o.WriteDSFPlacement do
	(
		if o.CaptureType == (PBXCaptureStr.GetCaptureTypes())[1] do
		(
			polShapeKnots = #()
			tmpGeo = PBXCaptureStr.GetGeometry o

			pos = GeoClass.GetGeoPos (meshop.getVert tmpGeo 1)
			append polShapeKnots (
				KnotData \
				Lat:pos.lat \
				Lon:pos.lon \
				uv:(point2 0 0)
				)
				
			pos = GeoClass.GetGeoPos (meshop.getVert tmpGeo 2)
			append polShapeKnots (
				KnotData \
				Lat:pos.lat \
				Lon:pos.lon \
				uv:(point2 1 0)
				)
				
			pos = GeoClass.GetGeoPos (meshop.getVert tmpGeo 4)
			append polShapeKnots (
				KnotData \
				Lat:pos.lat \
				Lon:pos.lon \
				uv:(point2 1 1)
				)
				
			pos = GeoClass.GetGeoPos (meshop.getVert tmpGeo 3)
			append polShapeKnots (
				KnotData \
				Lat:pos.lat \
				Lon:pos.lon \
				uv:(point2 0 1)
				)
				

			
			ExpDSF.AddPOLDef POLFilename:(PBXNodeStr.GetExportName o) POLSubPath:POLStdSaveSubPath

			ExpDSF.AddPOLPlacement Knots:polShapeKnots PointType:"mappedcorner" closed:True
				
			try delete tmpGeo catch()

		)
	)
)


DSFTXTFile = ExpDSF.WriteDsfTxt ()

-- Convert DSFtxt to a real DSF
DSFPath = ExportPath + DSFStdSaveSubPath + (PathingClass.DifferenceFromTo (getFilenamePath DSFTXTFile) (ExportPath + TMPStdSaveSubPath))
PBXXPlaneDSF.ConvertDsfTxtToDsf DSFTXTFile DSFPath
	
Destroydialog PBXXplaneDSFExportStatusPrompt