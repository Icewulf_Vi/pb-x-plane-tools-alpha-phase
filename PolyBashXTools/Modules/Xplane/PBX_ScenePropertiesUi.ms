
rollout PBXScenePropertiesUi "Scene Properties"
(
	local PropClass = PBXPropStr
	
	local vCity = (PropClass.getFileProp "AirportCity")
	local vCountry = (PropClass.getFileProp "AirportCountry")
	
	editText City "City" text:vCity
	editText Country "Country"text:vCountry
	
	on City changed arg do
	(
		PropClass.SetFileProp "City" arg

	)
	
		on Country changed arg do
	(
		PropClass.SetFileProp "Country" arg

	)
)
