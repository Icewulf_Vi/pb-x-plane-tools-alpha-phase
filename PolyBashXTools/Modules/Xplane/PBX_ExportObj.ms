setCommandPanelTaskMode #create
-- Basic Setup
PBXStaticObject_TypeCode = "PBXSOBJ"
PBXCapture_TypeCode = "PBXCAP"

--Load Classes
FileIn ((getFilenamePath (getThisScriptFilename())) + "PBX_XPlaneFileFormats.ms")
PathingClass = PBFilePathing ()
PropClass = PBXPropStr ()
MattClass = PBMaterialAndTexture()

-- SetVars
NodesToExport = #()
ExportPath = (PropClass.getFileProp "XPlaneExportPath")

TMPStdSaveSubPath = "ExportTMP\\"
OBJStdSaveSubPath = "Resources\\Objects\\"
CAPStdSaveSubPath = "Resources\\Objects\\"

Print "Start export Objects To Xplane"
	
Rollout PBXXplaneObjExportStatusPrompt "PBXXplaneObjExport"
(
	Label Lb "Writing objdata"
	Progressbar PbStatus value:0.0
)

TmpStaticObjects = PBXNodeStr.GetByTypeCode PBXStaticObject_TypeCode
TmpCaptureObjects = PBXNodeStr.GetByTypeCode PBXCapture_TypeCode
NodeCount = (TmpStaticObjects.count + TmpCaptureObjects.count)
CurrentNode = 0

createdialog PBXXplaneObjExportStatusPrompt





--##################
-- Export Static Objects to OBJ
--##################################################
for o in (TmpStaticObjects) do
(
	CurrentNode +=1 
	PBXXplaneObjExportStatusPrompt.PbStatus.value = ((CurrentNode as float)/(NodeCount as float) * 100)
	if o.WriteObj do
	(
		print ("Export:" + o.name)
		try
		(
			numSubs = PBXNodeTexture.GetNumSubs o
			
			for i = 1 to numSubs do
			(
				ID = undefined
				if NumSubs == 1 then
				(
					ID = undefined
				)
				else
				(
					ID = i
				)
				
				DiffuseTexture = undefined
				LitTexture = undefined
				NormalTexture = undefined
				MaxOpacMap = undefined

					-- Make Texture File
				--GetTextures 
				MaxMaterialName = (PBXNodeTexture.GetMap o "MaterialName" ID:ID)
				if MaxMaterialName != undefined do
				(
					OutTextureRes = (PBXNodeTexture.GetSize o ID:ID)
					
					-- Combine Maps
					MaxDiffMap = (PBXNodeTexture.GetQueueMap o "Diffuse" ID:ID)
					
					MaxOpacMap = (PBXNodeTexture.GetQueueMap o "Opacity" ID:ID)
					MaxNormMap = (PBXNodeTexture.GetQueueMap o "Normal" ID:ID)
					MaxSpecMap = (PBXNodeTexture.GetQueueMap o "Specular" ID:ID)
					MaxSeIlMap = (PBXNodeTexture.GetQueueMap o "selfillum"ID:ID)
					
				
					if MaxDiffMap != undefined or MaxOpacMap != undefined do
					(
						Local DiffTextureName = (ExportPath + TMPStdSaveSubPath + MaxMaterialName +"_Diff.png")
						Rasterizer = #(MaxDiffMap,MaxOpacMap,(PBXNodeTexture.getSize o),DiffTextureName)
					--	print Rasterizer
						append PBXMapQueue.PBXQuedMapRenderBitmap Rasterizer
						
						QueCode = "PBXTempA = \"" + (DiffTextureName as string) + "\"" + "\n"
						QueCode +="PBXTempB = \"" + ((pathConfig.resolvePathSymbols (ExportPath + OBJStdSaveSubPath))) as string + "\"" + "\n"
						QueCode += "PBXXplaneDataCoverter.ConvertToXplaneDDS PBXTempA PBXTempB"

						append PBXMapQueue.PBXMapQueScriptCode (ScriptString.Convert QueCode)

						DiffuseTexture = (ExportPath + OBJStdSaveSubPath + MaxMaterialName +"_Diff.dds")
					)
					
					if MaxSeIlMap != undefined do
					(
						Local LitTextureName = (ExportPath + TMPStdSaveSubPath + MaxMaterialName +"_Lit.png")
						Rasterizer = #(MaxSeIlMap,MaxOpacMap,(PBXNodeTexture.getSize o),LitTextureName)
						--print Rasterizer
						append PBXMapQueue.PBXQuedMapRenderBitmap Rasterizer
						

						QueCode = "PBXTempA = \"" + toupper (LitTextureName as string) + "\"" + "\n"
						QueCode +="PBXTempB = \"" + ((pathConfig.resolvePathSymbols (ExportPath + OBJStdSaveSubPath))) as string + "\"" + "\n"
						QueCode += "PBXXplaneDataCoverter.ConvertToXplaneDDS PBXTempA PBXTempB"

						append PBXMapQueue.PBXMapQueScriptCode (ScriptString.Convert QueCode)

						LitTexture = (ExportPath + OBJStdSaveSubPath + MaxMaterialName +"_Lit.dds")
					)

					if MaxNormMap != undefined or MaxSpecMap != undefined do
					(
						Local NormalTextureName = (ExportPath + TMPStdSaveSubPath + MaxMaterialName +"_Norm.png")
						Rasterizer = #(MaxNormMap,MaxSpecMap,(PBXNodeTexture.getSize o),NormalTextureName)
						--print Rasterizer
						append PBXMapQueue.PBXQuedMapRenderBitmap Rasterizer
						
						QueCode = "PBXTempA = \"" + toupper (NormalTextureName as string) + "\"" + "\n"
						QueCode +="PBXTempB = \"" + ((pathConfig.resolvePathSymbols (ExportPath + OBJStdSaveSubPath))) as string + "\"" + "\n"
						QueCode += "PBXXplaneDataCoverter.ConvertToXplaneDDS PBXTempA PBXTempB"

						append PBXMapQueue.PBXMapQueScriptCode (ScriptString.Convert QueCode)
						NormalTexture = (ExportPath + OBJStdSaveSubPath + MaxMaterialName +"_Norm.dds")
					)
				)
				
				
				-- Set OBJ File Export Properties
					-- Get data from Xnode
				
				-- Get Lights
				LightObjects = undefined
				if ID == 1 or ID == undefined do
				(
					LightObjects = PBXStaticObjectStr.GetAllLights o
					print LightObjects
				)
				
				-- realy noBlend needed by splitted meshes?
				tNoBlend = o.useNoBlend
				if ID != undefined and MaxOpacMap == undefined do
					tNoBlend = false
					
				ExportGeometry = (PBXStaticObjectStr.CastGeometry o OneMesh:False ID:ID)
				ExpOBJ = PBXXPlaneObj \
				FileName:(PBXNodeStr.GetExportName o ID:ID) \
				DiffuseTexture: DiffuseTexture \
				LitTexture: LitTexture \
				NormalTexture: NormalTexture \
				FileSavePath:(ExportPath + OBJStdSaveSubPath) \
				Geometry:ExportGeometry \
				Draped:o.Draped \
				UseGlobals: o.UseGlobals \
				NoBlend: tNoBlend \
				NoBlendValue: o.NoBlendValue \
				UseLod: o.Uselod \
				MinLod: o.Lowlod \
				MaxLod: o.Highlod \
				UseLayer: o.UseLayer \
				Layer: o.Layer \
				LayerGroup: o.Layergroup \
				MappingChannel: (PBXNodeTexture.GetMappingChannel o ID:ID)  \
				CoordinateSystemTarget:o \
				LightNodes:LightObjects
				
				-- Write OBJ File
				ExpOBJ.WriteOBJ ()
				
				for ge in ExportGeometry do
				(
					try
					(
						delete ge
					)
					catch (print (ge + " cannot be deleted"))
				)
			)
		)
		catch
		(
			print ("Error:Exporting " + o.name + " failed.")
		)
	)
)

--## Captures
for o in (TmpCaptureObjects) do
(
	CurrentNode +=1 
	PBXXplaneObjExportStatusPrompt.PbStatus.value = ((CurrentNode as float)/(NodeCount as float) * 100)
	if o.WriteAsOBJ do
	(
		print ("Export:" + o.name)
		try
		(
			if o.CaptureType == (PBXCaptureStr.GetCaptureTypes())[1] do
			(
				MaxMaterialName = PBXNodeStr.GetExportName o
				if MaxMaterialName != undefined do
				(
					MaxDiffMap = (PBXNodeTexture.GetMap o "Diffuse")
					MaxOpacMap = (PBXNodeTexture.GetMap o "Opacity")
					MaxNormMap = (PBXNodeTexture.GetMap o "Normal")
					MaxSpecMap = (PBXNodeTexture.GetMap o "Specular")
					MaxSeIlMap = (PBXNodeTexture.GetMap o "SelfIllum")
					
					OutTextureRes = (PBXNodeTexture.getSize o)
					DiffuseMapName = "NotAssigned"
					if MaxDiffMap != undefined or MaxOpacMap != undefined do
					(
						DiffTex = MattClass.RenderBitmap \
						MaxDiffMap \
						MaxOpacMap \
						OutTextureRes \
						(ExportPath + TMPStdSaveSubPath + MaxMaterialName +"_Diff.png")
						
						-- Convert Texture to dds and add it to the ExportObj
						DiffuseMapName = PBXXplaneDataCoverter.ConvertToXplaneDDS DiffTex (ExportPath + CAPStdSaveSubPath)
					)
					
					if MaxSeIlMap != undefined do
					(
						LitTex = MattClass.RenderBitmap \
						MaxSeIlMap \
						MaxOpacMap \
						OutTextureRes \
						(ExportPath + TMPStdSaveSubPath + MaxMaterialName +"_Lit.png")
						
						LitMapName = PBXXplaneDataCoverter.ConvertToXplaneDDS LitTex (ExportPath + CAPStdSaveSubPath)
					)

					if MaxNormMap != undefined or MaxSpecMap != undefined do
					(
						-- Process Normal and Specular 
						NormalTex = MattClass.RenderBitmap \
						MaxNormMap \
						MaxSpecMap \
						OutTextureRes \
						(ExportPath + TMPStdSaveSubPath + MaxMaterialName +"_Norm.png")
						
						NormalMapName = PBXXplaneDataCoverter.ConvertToXplaneDDS NormalTex (ExportPath + CAPStdSaveSubPath)
					)
				)
				
				-- Set OBJ File Export Properties
				
				-- realy noBlend needed by splitted meshes?
				tNoBlend = o.useNoBlend
				if MaxOpacMap == undefined do
					tNoBlend = false
				
				if MaxOpacMap != undefined do noBlend = true
				TmpGeo = PBXCaptureStr.CastGeometry o onemesh:true
				
				ExpOBJ = PBXXPlaneObj \
				FileName:(PBXNodeStr.GetExportName o) \
				FileSavePath:(ExportPath + CAPStdSaveSubPath) \
				Geometry:(PBXCaptureStr.GetGeometry o) \
				Draped:true \
				UseGlobals: false \
				NoBlend: noBlend \
				NoBlendValue: 0.5 \
				UseLayer: o.UseLayer \
				Layer: o.Layer \
				LayerGroup: o.Layergroup \
				MappingChannel: 1 \
				DiffuseTexture: DiffuseMapName \
				LitTexture: LitMapName \
				NormalTexture: NormalMapName \
				CoordinateSystemTarget:o \
				
				-- Write OBJ File
				ExpOBJ.WriteOBJ ()
				
				for k in tmpGeo do try delete k catch()
			)
		)
		catch
		(
			print ("Error:Exporting " + o.name + "failed.")
		)
	)
)

DestroyDialog PBXXplaneObjExportStatusPrompt
Print "Finisched export"
