setCommandPanelTaskMode #create
-- Basic Setup
PBXLine_TypeCode = "PBXLIN"
PBXCapture_TypeCode = "PBXCAP"

--Load Classes
FileIn ((getFilenamePath (getThisScriptFilename())) + "PBX_XPlaneFileFormats.ms")
PathingClass = PBFilePathing ()
PropClass = PBXPropStr ()
MattClass =	PBMaterialAndTexture()
GeoClass = PBGeoCordStr \
	WorldZeroCordLat:(PropClass.getFileProp "WorldZeroCordLat") \
	WorldZeroCordLon:(PropClass.getFileProp "WorldZeroCordLon") \
	WorldZeroCordAlt:(PropClass.getFileProp "WorldZeroCordAlt") 
	
-- SetVars
NodesToExport = #()
ExportPath = (PropClass.getFileProp "XPlaneExportPath")

TMPStdSaveSubPath = "ExportTMP\\"
LINStdSaveSubPath = "Resources\\Objects\\"

Print "Start export Lines to Xplane"

Rollout PBXXplaneLinExportStatusPrompt "PBXXplaneLinExport"
(
	Label Lb "Writing Lin data"
	Progressbar PbStatus value:0.0
)
	
TmpLineObjects = PBXNodeStr.GetByTypeCode PBXLine_TypeCode
NodeCount = TmpLineObjects.count
CurrentNode = 0

createdialog PBXXplaneLinExportStatusPrompt


--###############################
-- Lines
--##################################################
for o in TmpLineObjects do
(
	
	CurrentNode +=1 
	PBXXplaneLinExportStatusPrompt.PbStatus.value = ((CurrentNode as float)/(NodeCount as float) * 100)
	
	if o.WriteLin and (PBXLineObjectStr.HasEdgeOverlay o) do
	(
		print ("Export:" + o.name)
--		try
--		(
			-- Get Texture Informations
			MaxMaterialName = (PBXNodeTexture.GetMap o "MaterialName" ID:1)
			if MaxMaterialName != undefined do
			(
				MaxDiffMap = (PBXNodeTexture.GetQueueMap o "Diffuse" ID:1)
				MaxOpacMap = (PBXNodeTexture.GetQueueMap o "Opacity" ID:1)
				OutTextureRes = (PBXNodeTexture.getSize o ID:1)
				
				DiffuseTexture = (ExportPath + LINStdSaveSubPath + MaxMaterialName +"_Diff.dds")
				if MaxDiffMap != undefined or MaxOpacMap != undefined do
				(
					
					Local DiffTextureName = (ExportPath + TMPStdSaveSubPath + MaxMaterialName +"_Diff.png")
					Rasterizer = #(MaxDiffMap,MaxOpacMap,(PBXNodeTexture.getSize o),DiffTextureName)
					--print Rasterizer
					append PBXMapQueue.PBXQuedMapRenderBitmap Rasterizer
					
					append PBXMapQueue.PBXMapQueScriptCode (ScriptString.Convert( \
						"PBXTempA = \"" + DiffTextureName as string + "\" "  \
						+"PBXTempB = \"" + (ExportPath + LINStdSaveSubPath) as string + "\" "  \
						+"PBXXplaneDataCoverter.ConvertToXplaneDDS PBXTempA PBXTempB  "))
						--print (ExportPath + LINStdSaveSubPath)
					DiffuseTexture = (ExportPath + LINStdSaveSubPath + MaxMaterialName +"_Diff.dds")
				)
				-- Make Lin File
				TheLine = PBXXPlaneLIN \
				Filename:(PBXNodeStr.GetExportName o) \
				FileSavePath:(ExportPath + LINStdSaveSubPath) \
				TextureWidth:OutTextureRes.x \
				WorldScale:o.LineSize \
				UseLayer: o.UseLayer \
				Layer:o.Layer \
				LayerGroup: o.Layergroup \
				DiffuseTexture:DiffuseTexture
				
				

				for LineItem in (PBXLineObjectStr.GetItems o) do
				(
					if LineItem.EdgeOverlay do
					(
						CenterU = LineItem.EdgeCutLeft + ((LineItem.EdgeCutRight - LineItem.EdgeCutLeft)/2)
						print(LineItem.EdgeCutRight  as string +" "+ LineItem.EdgeCutLeft  as string )
						print(CenterU as string)
						
						CenterU += (LineItem.Shift / o.LineSize)
						TheLine.AddLine LeftBoundU:LineItem.EdgeCutLeft CenterU:CenterU RightBoundU:LineItem.EdgeCutRight TextureWidth:OutTextureRes.x
					)
				)
				
				print (TheLine.GetXplaneLineCode ())
				
				TheLine.WriteLine()
			)
--		)
--		catch
--		(
--			print ("Error:Exporting " + o.name + "failed.")
--		)
	)
)
--##################################################

destroydialog PBXXplaneLinExportStatusPrompt