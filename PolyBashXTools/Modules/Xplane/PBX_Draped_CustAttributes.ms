PBX_SOBJNode_CustAttributes = attributes "PBXSOBJ_XplaneDrapedLayer_CustAtt"
(
	Parameters main rollout:params
	(
		Draped Type:#Boolean ui:CbDraped
	)
	
	Rollout Params "X Plane Draped Properties"
	(
		----------- Draped ---------------
		checkbox CbDraped "Draped" state:($.Draped) triState:2
		--------------------------------------------
	)
)