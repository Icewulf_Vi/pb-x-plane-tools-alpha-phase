Struct PBXXplaneDataCoverter
(

	--- This is not working korrect must be modified Xplane Rotation is the same as max but *-1
	Fn CropRotation rotValue =
	(
		rotValue = rotValue *-1 
		if rotValue < 0 do
		(
			multiplikator = ((rotValue/360)*-1) as integer
			rotValue = rotValue + ((multiplikator+1)*360)
			
			return rotValue
		)
		
		if rotValue >= 360 do
		(
			multiplikator = (rotValue/360) as integer
			rotValue = rotValue - (multiplikator*360)
			
			return rotValue
		)
		
		return rotValue
	),
	
	Fn WrapPathToXplanePath Path =
	(
		 for l = 1 to path.count do 
		(
			if Path[l] == "\\" do Path[l] = "/"
		)
		return path
	),
	
	-- Wrap Max XYZ Coordinates To Xplane XYZ Coordinates
	FN WrapMaxXYZCoordinatesToOpenGL Coordinates =
	(
		return #(Coordinates[1],Coordinates[3],Coordinates[2])
	),

	FN ConvertToXplaneDDS InTextureFile OutSavePath =
	(
print ("MakeDir:" + OutSavePath)
		MakeDir OutSavePath
		--Convert png To DDS
print ("Launch DDSTool.exe:" + InTextureFile + " to " + OutSavePath)
		command = ""
		command += ("\"" + (pathConfig.removePathLeaf (getThisScriptFilename())) + "\\DDSTool.exe" + "\"")
		command += " --png2dxt --std_mips --gamma_22 --scale_none "
		command += ("\"" + InTextureFile + "\"")  
		command += (" ")  
		command += ("\"" + OutSavePath + (getFilenameFile InTextureFile) + ".dds" + "\"")
		command += "\n"
		--command += "Pause"
		ShellLaunch  (PBWinControll.WriteBatFile ((pathConfig.removePathLeaf InTextureFile) + "\\Convert" + (getFilenameFile InTextureFile) + ".bat") command True) ""

		Return ((OutSavePath + (getFilenameFile InTextureFile)) + ".dds")
	),
	
	fn UVToXplaneLine UMapping TextureWidth =
	(
		return UMapping * TextureWidth
	),
	
	fn MaxScaleToXplaneLine value =
	(
		if classof value == point2 do return value
		if classof value == float do return point2 value value
		if classof value == integer do return point2 (value as float) (value as float)
	)
	
)

struct PBXXPlaneLIN
(
	-- Pathes
	FileName = undefined,
	FileSavePath = undefined,
	
	--Textures
 	DiffuseTexture = undefined,
	NormalTexture = undefined,
	
	--Properties
	TextureWidth = undefined,
	WorldScale = undefined,
	Mirrored = undefined,
	
	LineLayerCode = "",
	LineID = 0,
	
	-- Layer
	UseLayer = False,
	LayerGroup = undefined,
	Layer = 0,
	
	
	Fn GetXplaneLineCode =
	(
		LineCode = ""
		LineCode += "A \n" \
			+"850 \n" \
			+"LINE_PAINT \n" \
		
		-- Texture
		if DiffuseTexture != undefined then LineCode += "TEXTURE "+ (filenameFromPath DiffuseTexture) as String + "\n"
		if NormalTexture != undefined then LineCode += "TEXTURE_NORMAL "+ (filenameFromPath NormalTexture) as String + " 1.0" +"\n"
		
		-- Layer
		if UseLayer == True and LayerGroup != undefined do (LineCode += "LAYER_GROUP " + LayerGroup +" "+ (Layer as string) + "\n")
		
		-- TEX_WIDTH
		if TextureWidth != undefined do LineCode += "TEX_WIDTH " + TextureWidth as string + "\n"
		
		-- Scale
		if WorldScale != undefined do 
		(	
			WorldScale = PBXXplaneDataCoverter.MaxScaleToXplaneLine WorldScale
			LineCode += "SCALE " + (WorldScale.x) as string +" "+ (WorldScale.y) as string + "\n"
		)
		
		-- Mirrored
		if Mirrored == True do LineCode += "MIRROR" + "\n"
			
		LineCode += LineLayerCode
			
		return LineCode
		
	),
	
	Fn GetXplaneLineLayerCode ID:0 LeftBoundU:0.0 CenterU:0.5 RightBoundU:1.0 TextureWidth:512 =
	(
		
		LeftBound = PBXXplaneDataCoverter.UVToXplaneLine LeftBoundU TextureWidth 
		Center = PBXXplaneDataCoverter.UVToXplaneLine CenterU TextureWidth 
		RightBound = PBXXplaneDataCoverter.UVToXplaneLine RightBoundU TextureWidth 
		
		
		a = ""
		a += "S_OFFSET "
		a += (ID as integer) as string 
		a += " "
		a += (LeftBound as float) as string
		a += " "
		a += (Center as float) as string
		a += " "
		a += (RightBound as float) as string 
		a += " \n"
		return a
		
	),
	
	Fn AddLine LeftBoundU:0.0 CenterU:0.5 RightBoundU:1.0 TextureWidth:512 =
	(
		LineLayerCode += "\n"
		LineLayerCode += GetXplaneLineLayerCode ID:LineID LeftBoundU:LeftBoundU CenterU:CenterU RightBoundU:RightBoundU TextureWidth:TextureWidth
		LineID += 1
	),	
	
	Fn WriteLine = 
	(
		Code = GetXplaneLineCode ()
		if Code != False do
		(
			
			LinFilePath = (FileSavePath)
			makedir LinFilePath
			LinFileSavePath = (LinFilePath + FileName + ".lin")

			LinFile = createfile LinFileSavePath
			format "%" Code to:LinFile
			close LinFile
			return (PBXXplaneDataCoverter.WrapPathToXplanePath (FileName + ".lin"))
		)
	)

)

Struct PBXXPlanePOL
(
	-- Pathes
	FileName = undefined,
	FileSavePath = undefined,
	
	--Textures
 	DiffuseTexture = undefined,
	LitTexture = undefined,
	TextureNoWrap = undefined,
	noAlpha = undefined,
	
	
	WorldScale = undefined,
	
	LoadCenter = false,
	LoadCenterPos = undefined,
	LoadCenterDim = undefined,
	LoadCenterTextureSize = undefined,
	
	-- Layer
	UseLayer = False,
	LayerGroup = undefined,
	Layer = 0,
	SurfaceType = undefined,
	
	Fn GetXplaneDrapedPolygoneCode =
	(
		out = ""
		out += "A \n" \
			+"850 \n" \
			+"DRAPED_POLYGON \n\n" \
		
		-- Texture
		
		if TextureNoWrap == true then
		(
			if not LoadCenter do(
				if DiffuseTexture != undefined then out += "TEXTURE_NOWRWAP "+ (filenameFromPath DiffuseTexture) as String + "\n"
				if LitTexture != undefined then out += "TEXTURE_LIT_NOWRWAP "+ (filenameFromPath LitTexture) as String + "\n"
			)
			if DiffuseTexture != undefined then out += "TEXTURE "+ (filenameFromPath DiffuseTexture) as String + "\n"
			if LitTexture != undefined then out += "TEXTURE_LIT "+ (filenameFromPath LitTexture) as String + "\n"

		)
		else 
		(
			if DiffuseTexture != undefined then out += "TEXTURE "+ (filenameFromPath DiffuseTexture) as String + "\n"
			if LitTexture != undefined then out += "TEXTURE_LIT "+ (filenameFromPath LitTexture) as String + "\n"
		)
		
		-- Scale
		if WorldScale != undefined do 
		(	
			WorldScale = PBXXplaneDataCoverter.MaxScaleToXplaneLine WorldScale
			out += "SCALE " + (WorldScale.x) as string +" "+ (WorldScale.y) as string + "\n"
		)
		
		-- Layer
		if UseLayer == True and LayerGroup != undefined do (out += "ATTR_layer_group " + LayerGroup +" "+ (Layer as string) + "\n")
		
		
		if classof SurfaceType == string do (out += "SURFACE " + SurfaceType + "\n")
		
		if noAlpha == true do (out += "NO_ALPHA" + "\n")
		print "###WritePol###"
		
		if LoadCenter == true do
		(
			out += ("LOAD_CENTER " + LoadCenterPos.Lat as string +" "+ LoadCenterPos.Lon as string+" "+ (LoadCenterDim as integer) as string +" "+ (LoadCenterTextureSize as integer) as string + "\n")
		)
		
		return out
		
	),
	
	
	Fn WritePOL = 
	(
		Code = GetXplaneDrapedPolygoneCode ()
		if Code != False do
		(
			
			PolFilePath = (FileSavePath)
			makedir PolFilePath
			PolFileSavePath = (PolFilePath + FileName + ".pol")

			PolFile = createfile PolFileSavePath
			format "%" Code to:PolFile
			close PolFile
			return (PBXXplaneDataCoverter.WrapPathToXplanePath (FileName + ".pol"))
		)
	)
)

struct PBXXPlaneObj
(
	-- XplaneATTR
	UseGlobals = False,
	NoBlendValue = 0.5,
	Draped = False,
	UseLod = False,
	MinLod = 0.0,
	MaxLod = 500.0,
	UseLayer = False,
	LayerGroup = undefined,
	Layer = 0,
	NoBlend = undefined,
	LightNodes = undefined,
	
	-- Pathes
	FileName = undefined,
	FileSavePath = undefined,
	
	-- PosData
	AlignToGround = undefined,
	SitOnGround = undefined,
	MeshScale = [1.000,1.000,1.00],
	
	-- MeshData
	Geometry = undefined,
	MappingChannel = 2,
	NormalSMG = False,
	
	--Textures
 	DiffuseTexture = undefined,
	LitTexture = undefined,
	NormalTexture = undefined,
	
	CoordinateSystemTarget = undefined,
	
	Fn WriteXplaneOBJ inGeometry FileNamePath DiffTexture:undefined LitTexture:undefined NormalTexture:undefined LightNodes:undefined Draped:false LODValue:undefined LayerGroupValue:undefined Layer:0 NoBlendValue:undefined UseGlobals:False MappingChannel:1 CoordinateSystemTarget:undefined =
	(
		if inGeometry == undefined do return undefined
		
		-- load assambly
		try
		(
			dotnet.loadAssembly (getFilenamePath (getThisScriptFilename()) + "XplaneOBJ.dll")
		)
		catch
		(
			Print ("Error Loading " + (getFilenamePath (getThisScriptFilename()) + "XplaneOBJ.dll."))
			return undefined
		)
		
		-- Naking Xplane Object with DLL
		XplaneObject = DotNetObject "XplaneFileFormats.OBJFile"
		XplaneObject.Name = (getFilenameFile FileNamePath)
		
		-- textures
		if DiffTexture != undefined do
		(
			XplaneObject.Textures.Texture = filenameFromPath DiffTexture
		)
		if NormalTexture != undefined do
		(
			XplaneObject.Textures.Texture_Normal = filenameFromPath NormalTexture
		)
		if LitTexture != undefined do
		(
			XplaneObject.Textures.Texture_Lit = filenameFromPath LitTexture
		)
		
		-- Attributes -- 
		if NoBlendValue != undefined do
		(
			if UseGlobals then
				XplaneObject.ObjectAttributes.NoBlend = (dotnetclass "XplaneFileFormats.Tables.Calculation").Global
			else
				XplaneObject.ObjectAttributes.NoBlend = (dotnetclass "XplaneFileFormats.Tables.Calculation").Local
			
			XplaneObject.ObjectAttributes.NoBlendValue = NoBlendValue
		)

		if LODValue != undefined do
		(
			XplaneObject.ObjectAttributes.LOD = (dotnetclass "XplaneFileFormats.Tables.Calculation").Local
			
			tlodValue = DotNetObject "XplaneFileFormats.Value.Point2"
			tlodValue.X = LODValue.x
			tlodValue.Y = LODValue.y
			XplaneObject.ObjectAttributes.LODValue = tlodValue
		)
		
		if Draped == true do
		(
			XplaneObject.ObjectAttributes.Draped = (dotnetclass "XplaneFileFormats.Tables.Calculation").Local
		)
		
		if LayerGroupValue != undefined do
		(
			XplaneObject.ObjectAttributes.LayerGroup = (dotnetclass "XplaneFileFormats.Tables.Calculation").Local
			XplaneObject.ObjectAttributes.LayerGroupValue = LayerGroupValue
			XplaneObject.ObjectAttributes.LayerGroupLevel = Layer
		)
		
		-- Lights
		if LightNodes != undefined do
		(
			for l in LightNodes do
			(
				if classof l == Omnilight do
				(
					
					pos = l.position
					if  CoordinateSystemTarget != undefined  do
					(
						pos = in coordsys CoordinateSystemTarget l.pos
					)
					pos = DotNetObject "XplaneFileFormats.Value.Point3" pos.x pos.y pos.z
					coords = (dotnetclass "XplaneFileFormats.Coordinate.Systems").Max
					
					NamedLight = DotNetObject "XplaneFileFormats.Value.NamendLight"	l.name pos coords	
					XplaneObject.AddLight NamedLight
					dotNet.setLifetimeControl NamedLight #dotnet
				)
				
				if classof l == targetSpot do
				(
					
					pos = l.position
					norm = (l.dir*-1)
					if CoordinateSystemTarget != undefined do
					(
						pos =  in coordsys CoordinateSystemTarget l.pos
						norm = in coordsys CoordinateSystemTarget (l.dir*-1)
					)
					

					pos = DotNetObject "XplaneFileFormats.Value.Point3" pos.x pos.y pos.z
					norm = DotNetObject "XplaneFileFormats.Value.Point3" norm.x norm.y norm.z
					--norm = DotNetObject "XplaneFileFormats.Value.Point3" 2.0 2.0 2.0
					
					lColor = DotNetObject "XplaneFileFormats.Value.Point4" (l.rgb.red/256.0) (l.rgb.green/256.0) (l.rgb.blue/256.0) l.multiplier
					
					size = l.farAttenEnd * 1.4 as float
					--size = 5.0
					
					coords = (dotnetclass "XplaneFileFormats.Coordinate.Systems").Max
					
					--b = pi * (l.farAttenEnd * lscale) * (l.Falloff/180)
					--b = 2 * (l.farAttenEnd) * sin (l.falloff/2)

					
					rad = cos(l.falloff * 0.5) 
					
					SpillLight = DotNetObject "XplaneFileFormats.Value.CustomSpillLight" pos lColor size norm rad "none" coords
					XplaneObject.AddLight SpillLight
					dotNet.setLifetimeControl SpillLight #dotnet
				)
			)
				
		)

		
		-- Submeshes
		---  local Attributes and Loop Faces to record geometry
		for o in inGeometry do
		(
			-- Prepare 3D Data
			print "Create SubMesh"
			print o.name
			tmp = snapshot o
			
			Triangulate = Turn_to_Mesh()
			Triangulate.name = "Triangulate"
			addModifier tmp Triangulate
			Triangulate.useInvisibleEdges = on
			InstanceMgr.MakeModifiersUnique tmp Triangulate #individual
			if CoordinateSystemTarget != undefined do
			(
				--tmp.pivot 
				tmp.pos = tmp.pos - CoordinateSystemTarget.pos
				tmp.pivot = point3 0 0 0
				tmp.Rotation.Z_Rotation -= CoordinateSystemTarget.Rotation.Z_Rotation
				
			--	tmp.Rotation.X_Rotation = tmp.Rotation.X_Rotation - CoordinateSystemTarget.Rotation.X_Rotation
			--	tmp.Rotation.Y_Rotation = tmp.Rotation.Y_Rotation - CoordinateSystemTarget.Rotation.Y_Rotation
			)
			collapseStack tmp
			
			try
			(
				meshop.buildMapFaces tmp MappingChannel
			)	
			catch
			(
				print ("Warning:Object "+ o.name +" has no UVW Mapping, ObjectFile can�t be written.")
				delete tmp
				return undefined
			)
			
			-- 3D Data is correct
			if superclassof o == Geometryclass do
			(
				-- Make new mesh .netobject
				MeshObject = DotNetObject "XplaneFileFormats.Value.Mesh"  
				MeshObject.CoordinateSystem = (dotnetclass "XplaneFileFormats.Coordinate.Systems").Max
				
				-- Loop tris and add them to mesh
				numFaces = tmp.Faces.Count
				for f =  1 to numFaces do
				(
					FaceVerts = getFace tmp f
					TFaceVerts = meshop.getMapFace tmp MappingChannel f
					FaceSmoothing = getFaceSmoothGroup tmp f
						
					VertObject1 = DotNetObject "XplaneFileFormats.Value.Vertex"  \
					((meshop.getvert tmp FaceVerts[1]).x as string) \
					((meshop.getvert tmp FaceVerts[1]).y as string) \
					((meshop.getvert tmp FaceVerts[1]).z as string) \
					((getNormal tmp FaceVerts[1]).x as string) \
					((getNormal tmp FaceVerts[1]).y as string) \
					((getNormal tmp FaceVerts[1]).z as string) \
					((meshop.getMapVert tmp MappingChannel TFaceVerts[1]).x as string) \
					((meshop.getMapVert tmp MappingChannel TFaceVerts[1]).y as string)\
					(FaceSmoothing as string)
					
					VertObject2 = DotNetObject "XplaneFileFormats.Value.Vertex"  \
					((meshop.getvert tmp FaceVerts[2]).x as string) \
					((meshop.getvert tmp FaceVerts[2]).y as string) \
					((meshop.getvert tmp FaceVerts[2]).z as string) \
					((getNormal tmp FaceVerts[2]).x as string) \
					((getNormal tmp FaceVerts[2]).y as string) \
					((getNormal tmp FaceVerts[2]).z as string) \
					((meshop.getMapVert tmp MappingChannel TFaceVerts[2]).x as string) \
					((meshop.getMapVert tmp MappingChannel TFaceVerts[2]).y as string) \
					(FaceSmoothing as string)
					
					VertObject3 = DotNetObject "XplaneFileFormats.Value.Vertex"  \
					((meshop.getvert tmp FaceVerts[3]).x as string) \
					((meshop.getvert tmp FaceVerts[3]).y as string) \
					((meshop.getvert tmp FaceVerts[3]).z as string) \
					((getNormal tmp FaceVerts[3]).x as string) \
					((getNormal tmp FaceVerts[3]).y as string) \
					((getNormal tmp FaceVerts[3]).z as string) \
					((meshop.getMapVert tmp MappingChannel TFaceVerts[3]).x as string) \
					((meshop.getMapVert tmp MappingChannel TFaceVerts[3]).y as string) \
					(FaceSmoothing as string)
					
					TriObject = (DotNetObject "XplaneFileFormats.Value.Triangel" VertObject1 VertObject2 VertObject3)
					
					dotNet.setLifetimeControl VertObject2 #dotnet
					dotNet.setLifetimeControl VertObject1 #dotnet
					dotNet.setLifetimeControl VertObject3 #dotnet
					
					MeshObject.AddTriangel TriObject
					dotNet.setLifetimeControl TriObject #dotnet
				)
				
				delete tmp
				
				-- Make Attributes .Net
				Subattributes = DotNetObject "XplaneFileFormats.Tables.MeshAttributes"
				
				if (GetUserProp o "Draped") == true do
				(
					Subattributes.Draped = (dotnetclass "XplaneFileFormats.Tables.Calculation").Local
				)
				
				if (GetUserProp o "UseLayer") == true do
				(
					Subattributes.LayerGroup = (dotnetclass "XplaneFileFormats.Tables.Calculation").Local
					Subattributes.LayerGroupValue = (GetUserProp o "LayerGroup")
					Subattributes.LayerGroupLevel = 2
				)
				
				if (GetUserProp o "UseLod") == true do
				(
					Subattributes.LOD = (dotnetclass "XplaneFileFormats.Tables.Calculation").Local
					
					tlodValue = DotNetObject "XplaneFileFormats.Value.Point2"
					tlodValue.X = (GetUserProp o "LowLod")
					tlodValue.Y = (GetUserProp o "HighLod")
					Subattributes.LODValue = tlodValue
				)
				

				XplaneObject.AddMesh o.name MeshObject Subattributes
				dotNet.setLifetimeControl MeshObject #dotnet
				dotNet.setLifetimeControl subattributes #dotnet
			)
			
		)
		
		XplaneObject.WriteOBJ FileNamePath
		--XplaneObject.Save (FileNamePath + "xml")
		dotNet.setLifetimeControl XplaneObject #dotnet
	),

	Fn WriteOBJ =
	(
		-- Process properties
		if UseLod then
			tlodvalue = point2 MinLod MaxLod
		else
			tlodvalue = undefined
		
		
		if UseLayer then
			tlayervalue = LayerGroup
		else
			tlayervalue = undefined

		
		if NoBlend then
			NoBlendValue = NoBlendValue
		else
			NoBlendValue = undefined
		
		-- Process textures
		if DiffuseTexture != undefined then
			DiffuseTextureValue = filenameFromPath DiffuseTexture
		else
			DiffuseTextureValue = undefined
		
		
		if LitTexture  != undefined then
			LitTextureValue = filenameFromPath LitTexture
		else
			LitTextureValue = undefined
		
		
		if NormalTexture != undefined then
			NormalTextureValue = filenameFromPath NormalTexture
		else
			NormalTexture = undefined
		
		if LightNodes != undefined then
		(
			tLightNodes = LightNodes
		)
		else
		(
			tLightNodes = undefined
		)

		if isvalidnode CoordinateSystemTarget then
		(
			tCoordinateSystemTarget = CoordinateSystemTarget
		)
		else
		(
			tCoordinateSystemTarget = undefined
		)
		
		makedir FileSavePath

		WriteXplaneOBJ Geometry (FileSavePath + FileName + ".obj")  \
		DiffTexture:(DiffuseTextureValue) \
		LitTexture:(LitTexture) \
		NormalTexture:(NormalTexture) \ 
		LightNodes:tLightNodes \
		Draped:Draped \
		LODValue:tlodvalue \ 
		LayerGroupValue:tlayervalue \ 
		Layer:Layer \
		NoBlendValue:NoBlendValue \
		UseGlobals:UseGlobals \
		MappingChannel:MappingChannel \
		CoordinateSystemTarget:tCoordinateSystemTarget

		
		return (PBXXplaneDataCoverter.WrapPathToXplanePath (FileName + ".obj"))
	)
	
)

Struct PBXXPlaneAGP
(
	CopyRightText = "\n\n##by PolyBash XTools",
	
	FileName = undefined,
	FileSavePath = undefined,
	AGPIndexID = 0,
	AGPObjectDefs = #(),
	AGPObjectPlacements = #(),
	AGPTextureXY = (point2 1024 1024),
	WorldSize = 512,
	Texture = "NoTextureAssigned",
	HideTile = False,
	
	fn AddToAGPObjectDefs OBJFileName:"undefinedOBJFileName" ObjSubpath:""=
	(
		if (getFilenameType OBJFileName) == "" do
		(
			OBJFilename = (OBJFileName + ".obj")
		)
		
		append AGPObjectDefs ("OBJECT "+ (PBXXplaneDataCoverter.WrapPathToXplanePath ObjSubpath) + OBJFileName)
	),
	
	fn AddToAGPObjectPlacements X:0.0 Y:0.0 Heading:0.0 =
	(
		tmpX = (formattedPrint ( (X + WorldSize.x/2) * (AGPTextureXY.x / WorldSize.x) ) format:"1.2f")
		tmpY = (formattedPrint ( (y + WorldSize.x/2) * (AGPTextureXY.y/WorldSize.x) ) format:"1.2f")
		append AGPObjectPlacements ("OBJ_DRAPED " + tmpX +" "+ tmpY as string +" "+ Heading as string +" "+ AGPIndexID as string)
	),
	
	fn GetAGPCode =
	(
		CodeHeader = ""
		CodeHeader = 	"A \n" + \
						"1000 \n" + \
						"AG_POINT \n" + \
						"\n"

		Texturecode = ""
		Texturecode +="HIDE_TILES"
		--Texturecode += "TEXTURE " + Texture + "\n"
		
		-- Texture size width height
		Texturecode += "TEXTURE_SCALE " + ((AGPTextureXY.x as float) as string) +" " +((AGPTextureXY.y as float) as string) + "\n"
		
		-- ScaleFactor to real Xplane world
		-- means the meters for the complete texture
		Texturecode += "TEXTURE_WIDTH " + ((WorldSize.y as float) as string) + "\n"
		
		-- Hides the Draped Polygone from the Texture -- optional
		if HideTile == true then
		(
			HideTilesCode = "HIDE_TILES\n"
		)
		else
		(
			HideTilesCode = ""
		)
		
		NodesDef = ""
		for t in AGPObjectDefs do NodesDef += t + "\n"
			
		-- Auschnitt aus Textur zum platzieren der 3D Modelle und des Draped Poly in pixel 
		TileCode = "TILE 0.0 0.0 " + ((AGPTextureXY.x) as string) + " " + ((AGPTextureXY.x) as string)  + "\n" \
					+ "ROTATION 0\n"
		
		NodesPlacement = ""
		for t in AGPObjectPlacements do NodesPlacement += t + "\n"
			
		AnchorCode = "ANCHOR_PT " + ((AGPTextureXY.x/2) as string) + " " + ((AGPTextureXY.x/2 ) as string) + "\n" -- CenterPoint
		
		CodeEnd = CopyRightText
		
		return (CodeHeader + Texturecode + HideTilesCode + NodesDef + TileCode + NodesPlacement + AnchorCode + CodeEnd)
	),
	
	Fn WriteAGP =
	(
		Code = GetAGPCode ()
		
		makedir FileSavePath
		AGPFileSavePath = (FileSavePath + FileName + ".agp")

		AGPFile = createfile AGPFileSavePath
		format "%" Code to:AGPFile
		close AGPFile
		return (PBXXplaneDataCoverter.WrapPathToXplanePath (FileName + ".agp"))
	)


	
)

struct PBXXPlaneDSF
(
	CopyRightText = "\n\n##by PolyBash XTools",
	
	-- Pathes
	FileSavePath = undefined,
	
	DSFLat = undefined,
	DSFLon = undefined,
	DSFAlt = undefined,
	-- DSFFile
	DSFOBJIndexID = -1,
	DSFPOLYIndexID = -1,
	DSFObjectDefs = #(),
	DSFObjectPlacements = #(),
	
	
	fn GetDsfFileName =
	(
		Lat = DSFLon
		Lon = DSFLat
		Alt = DSFAlt
		
		Latitudebound = Lat as integer
		if Latitudebound <= 0 do (Latitudebound -= 1 )
		LongitudeBound = Lon as Integer
		
		-- Build name
		LatFo = Latitudebound as string
		while LatFo.count <= 2 do
		(
			LatFo = "0" + LatFo
		)
		
		if Latitudebound >= 0 then Latpre = "+"
		else Latpre = ""
		
		LonFo = LongitudeBound as string
		while LonFo.count <= 1 do
		(
			LonFo = "0" + LonFo
		)
		
		if LongitudeBound >= 0 then Lonpre = "+"
		else Lonpre = ""
		
		return (Lonpre+LonFo + Latpre+LatFo) as string
	),
	
	fn GetDsfFolderName =
	(
		fname = (GetDsfFileName ())
		fname[3] = "0"
		if fname[4] == "-" then
		(
			Nfname = ((fname[5] + fname[6] + fname[7]) as integer) + 10
			
			fname[5] = (Nfname as string)[1]
			fname[6] = (Nfname as string)[2]
			fname[7] = "0"
		)
		else fname[7] = "0"
		
		return fname
	),
	
	fn GetAutogenPointDef AGPFileName:"undefinedAGPFileName" AGPSubpath:""=
	(
		if (tolower (getFilenameType AGPFileName)) == "" do
		(
			AGPFilename = (AGPFileName + ".agp")
		)
		
		return ("OBJECT_DEF "+ (PBXXplaneDataCoverter.WrapPathToXplanePath AGPSubpath) + AGPFileName)
	),
	
	fn AddAutogenPointDef AGPFileName:"undefinedAGPFileName" AGPSubpath:""=
	(
		append DSFObjectDefs (GetAutogenPointDef AGPFileName:AGPFileName AGPSubpath:AGPSubpath)
		DSFOBJIndexID += 1
	),
	
	fn GetAutogenPointPlacement GeoCordinates:undefined Heading:0.0 SitOnGround:True =
	(
		if GeoCordinates == undefined do return ""
		if SitOnGround == True then
		(
			return ("OBJECT " + DSFOBJIndexID as string +" "+ GeoCordinates.Lon +" "+ GeoCordinates.Lat + " " + (PBXXplaneDataCoverter.CropRotation Heading) as string)
		)
		else
		(
			return ("OBJECT_MSL " + DSFOBJIndexID as string +" "+ GeoCordinates.Lon +" "+ GeoCordinates.Lat +" "+ GeoCordinates.Alt +" "+ (PBXXplaneDataCoverter.CropRotation Heading) as string)
		)
	),
	
	fn AddAutogenPointPlacement GeoCordinates:undefined Heading:0.0 SitOnGround:True =
	(
		append DSFObjectPlacements (GetAutogenPointPlacement GeoCordinates:GeoCordinates Heading:Heading SitOnGround:SitOnGround)
	),
	
	fn GetDSFObjectDef OBJFileName:"undefinedOBJFileName" ObjSubpath:""=
	(
		if (tolower (getFilenameType OBJFileName)) == "" do
		(
			OBJFilename = (OBJFileName + ".obj")
		)
		
		return ("OBJECT_DEF "+ (PBXXplaneDataCoverter.WrapPathToXplanePath ObjSubpath) + OBJFileName)
	),
	
	fn AddObjectDef OBJFileName:"undefinedOBJFileName" ObjSubpath:""=
	(
		append DSFObjectDefs (GetDSFObjectDef OBJFileName:OBJFileName ObjSubpath:ObjSubpath)
		DSFOBJIndexID += 1
	),
	
	fn GetObjectPlacement GeoCordinates:undefined Heading:0.0 SitOnGround:True =
	(
		if GeoCordinates == undefined do return ""
		if  SitOnGround == True then
		(
			return ("OBJECT " + DSFOBJIndexID as string +" "+ GeoCordinates.Lon +" "+ GeoCordinates.Lat + " " + (PBXXplaneDataCoverter.CropRotation Heading) as string)
		)
		else
		(
			return ("OBJECT_MSL " + DSFOBJIndexID as string +" "+ GeoCordinates.Lon +" "+ GeoCordinates.Lat +" "+GeoCordinates.Alt +" "+ (PBXXplaneDataCoverter.CropRotation Heading) as string)
		)
	),
	
	fn AddObjectPlacement GeoCordinates:undefined Heading:0.0 SitOnGround:True =
	(
		append DSFObjectPlacements (GetObjectPlacement GeoCordinates:GeoCordinates Heading:Heading SitOnGround:SitOnGround)
	),
	
	Fn GetDSFLineDef PolygoneFilename:"UndefinedPolygoneFileName" LineSubPath:"" =
	(
		
		if (tolower (getFilenameType PolygoneFilename)) == "" do
		(
			PolygoneFilename = (PolygoneFilename + ".lin")
		)
		
		return ("POLYGON_DEF " + (PBXXplaneDataCoverter.WrapPathToXplanePath LineSubPath) + PolygoneFilename)
	),
	
	Fn AddLineDef PolygoneFilename:"UndefinedPolygoneFileName" LineSubPath:"" =
	(
		append DSFObjectDefs (GetDSFLineDef PolygoneFilename:PolygoneFilename LineSubPath:LineSubPath)
		DSFPOLYIndexID += 1
	),
	
	Fn GetDSFPOLDef POLFilename:"UndefinedPOLFileName" PolSubPath:"" =
	(
		
		if (tolower (getFilenameType POLFilename)) == "" do
		(
			POLFilename = (POLFilename + ".pol")
		)
		
		return ("POLYGON_DEF " + (PBXXplaneDataCoverter.WrapPathToXplanePath PolSubPath) + POLFilename)
	),
	
	Fn AddPolDef POLFilename:"UndefinedPOLFileName" PolSubPath:"" =
	(
		DSFPOLYIndexID += 1
		append DSFObjectDefs (GetDSFPOLDef POLFilename:POLFilename PolSubPath:PolSubPath)
	),
	
	Fn GetLinePlacement IndexID:0 Knots:undefined PointType:"corner" closed:False =
	(
		out = "BEGIN_POLYGON "
		out += IndexID as string
		if closed == true then out+= " 2 " else out+= " 0 "

		if tolower PointType == "corner" do
		(
			out += "2"
			out += "\n"
			out += "BEGIN_WINDING\n"
			for p in Knots do
			(
				out += "POLYGON_POINT " +  p.lon + " " + p.lat + "\n"
				
				sddasdsa
			)
		)
		
		if tolower PointType == "smooth" do
		(
			out += "4"
			out += "\n"
			out += "BEGIN_WINDING\n"
			for p in Knots do
			(
				out += "POLYGON_POINT " +  p.lon + " " + p.lat +" "+ p.ovlon +" "+ p.ovlat + "\n"
			)
		)
		
		if tolower PointType == "bezier" do
		(
			out += "4"
			out += "\n"
			out += "BEGIN_WINDING\n"
			
			for i = 1 to Knots.count do
			(
				p = Knots[i]
				
				-- firstpoint
				if i == 1 do 
				(
					--
					out += "POLYGON_POINT " +  p.lon + " " + p.lat +" "+ p.ovlon + " " + p.ovlat + "\n" 
				)
				-- points
				
				
				if i != 1 and i != Knotcount do
				(
					--Knot*
					--??????? BrakeBezPos =  2*KnotPos - (getinVec CalcShape S K) ??????
					out += "POLYGON_POINT " +  p.lon + " " + p.lat +" " + p.ovlon + " " + p.ovlat + "\n"             --+ BezBrakepos[1] + " " + BezBrakepos[2]
					--knot knot
					out += "POLYGON_POINT " +  p.lon + " " + p.lat +" "+ p.lon + " " + p.lat + "\n" 
					--knotouthandle
					out += "POLYGON_POINT " +  p.lon + " " + p.lat +" "+ p.ovlon + " " + p.ovlat + "\n" 
				)
				
				--[21.11.2012 20:38:54] bsupnik: IF the _previous_ edge is straight you can OMIT knot knot*
				--[21.11.2012 20:38:54] bsupnik: If the _following_ edge is staright you acn omit knot knot outhandel
				--[21.11.2012 20:39:00] bsupnik: and of course if they are both straight you must only write knot knot
				
				-- endpoint
				if i == Knots.count do 
				(
					--Knot*
					out += "POLYGON_POINT " +  p.lon + " " + p.lat + " " + p.ovlon + " " + p.ovlat + "\n" 
				)	
			)
		)
		
		out += "END_WINDING\n"
		out += "END_POLYGON \n"
	),
	
	Fn AddLinePlacement Knots:undefined PointType:"corner" closed:False =
	(
		append DSFObjectPlacements (GetLinePlacement IndexID:DSFPOLYIndexID Knots:Knots PointType:PointType closed:closed)
	),
	
	Fn GetPOLPlacement IndexID:0 Knots:undefined PointType:"corner" MappingRotation:0 =
	(
		
		
		
		out = "BEGIN_POLYGON "
		out += IndexID as string
		if tolower PointType == "mappedcorner" then  out+= " 65535 " else		
		out+= " " + MappingRotation as string + " "

		
		if tolower PointType == "mappedcorner" do
		(
			out += "4"
			out += "\n"
			out += "BEGIN_WINDING\n"
			for p in Knots do
			(
				out += "POLYGON_POINT " +  p.lon + " " + p.lat +" "+ p.UV.x as string +" "+ p.UV.y as string + "\n"
				
				sddasdsa
			)
		)
		
		if tolower PointType == "corner" do
		(
			out += "2"
			out += "\n"
			out += "BEGIN_WINDING\n"
			for p in Knots do
			(
				out += "POLYGON_POINT " +  p.lon + " " + p.lat + "\n"
				
				sddasdsa
			)
		)
		
		if tolower PointType == "smooth" do
		(
			out += "4"
			out += "\n"
			out += "BEGIN_WINDING\n"
			for p in Knots do
			(
				out += "POLYGON_POINT " +  p.lon + " " + p.lat +" "+ p.ovlon +" "+ p.ovlat + "\n"
			)
		)
		
		if tolower PointType == "bezier" do
		(
			out += "4"
			out += "\n"
			out += "BEGIN_WINDING\n"
			
			for i = 1 to Knots.count do
			(
				p = Knots[i]
				
				-- firstpoint
				if i == 1 do 
				(
					--
					out += "POLYGON_POINT " +  p.lon + " " + p.lat +" "+ p.ovlon + " " + p.ovlat + "\n" 
				)
				-- points
				
				
				if i != 1 and i != Knotcount do
				(
					--Knot*
					--??????? BrakeBezPos =  2*KnotPos - (getinVec CalcShape S K) ??????
					out += "POLYGON_POINT " +  p.lon + " " + p.lat +" " + p.ovlon + " " + p.ovlat + "\n"             --+ BezBrakepos[1] + " " + BezBrakepos[2]
					--knot knot
					out += "POLYGON_POINT " +  p.lon + " " + p.lat +" "+ p.lon + " " + p.lat + "\n" 
					--knotouthandle
					out += "POLYGON_POINT " +  p.lon + " " + p.lat +" "+ p.ovlon + " " + p.ovlat + "\n" 
				)
				
				--[21.11.2012 20:38:54] bsupnik: IF the _previous_ edge is straight you can OMIT knot knot*
				--[21.11.2012 20:38:54] bsupnik: If the _following_ edge is staright you acn omit knot knot outhandel
				--[21.11.2012 20:39:00] bsupnik: and of course if they are both straight you must only write knot knot
				
				-- endpoint
				if i == Knots.count do 
				(
					--Knot*
					out += "POLYGON_POINT " +  p.lon + " " + p.lat + " " + p.ovlon + " " + p.ovlat + "\n" 
				)	
			)
		)
		
		out += "END_WINDING\n"
		out += "END_POLYGON \n"
	),
	
	Fn AddPOLPlacement Knots:undefined PointType:"corner" MappingRotation:0 =
	(
		append DSFObjectPlacements (GetPOLPlacement IndexID:DSFPOLYIndexID Knots:Knots PointType:PointType MappingRotation:MappingRotation)
	),
	
	fn GetDSFCode =
	(
		
		if DSFLat == undefined and DSFLon == undefined do return false
		
		CodeHeader = ""
		CodeHeader = 	"I \n" + \
						"800 \n" + \
						"DSF2TEXT \n" + \
						"\n"

		CodeHeader += 	"PROPERTY sim/planet earth\n" + \
						"PROPERTY sim/overlay 1\n"
		
		Latbound = DSFLon as integer
		Lonbound = DSFLat as integer
		
		SNBoundOffset = 1
		WEBoundOffset = 1
		
		if Latbound <= 0 then
		(
			CodeHeader += 	"PROPERTY sim/west " + (Latbound - WEBoundOffset) as string + "\n" + \
							"PROPERTY sim/east " + Latbound as string  + "\n"
		)
		else
		(
			CodeHeader += 	"PROPERTY sim/west " + Latbound as string + "\n" + \
							"PROPERTY sim/east " + (Latbound + WEBoundOffset) as string  + "\n"
		)
		
		CodeHeader += 	"PROPERTY sim/north " + (Lonbound + SNBoundOffset) as string + "\n" + \
						"PROPERTY sim/south " + Lonbound as string + "\n"
		
		RQWCode = ""
		RQWCode +="PROPERTY sim/require_object 1/0"  + "\n"
		RQWCode +="PROPERTY sim/require_agpoint 1/0"  + "\n"
		


		CodeDef = ""
		for t in DSFObjectDefs do CodeDef += t + "\n"
		
		CodePlacement = ""		
		for t in DSFObjectPlacements do CodeDef += t + "\n"
		
		CodeEnd = CopyRightText
		
		return (CodeHeader + RQWCode + CodeDef + CodePlacement + CodeEnd)
	),

	Fn WriteDsfTxt =
	(
		if FileSavePath == undefined do return false
		
		Code = GetDSFCode ()
		if code == false do return false
		
		DSFFilePath = (FileSavePath + (GetDsfFolderName ()) )
print ("MakeDir:" + (FileSavePath + (GetDsfFolderName ()) ))
		makedir DSFFilePath
print ("MakeFile:" + (DSFFilePath +"\\"+ (GetDsfFileName ())+ ".txt"))
		DSFFile = createfile (DSFFilePath +"\\"+ (GetDsfFileName ())+ ".txt")
		format "%" Code to:DSFFile
		close DSFFile
		
		return (DSFFilePath +"\\"+ (GetDsfFileName ())+ ".txt")
		
	),
	
	-- Calls DSFToolExe
	FN ConvertDsfTxtToDsf InDsfTxtFile OutSavePath =
	(
print ("MakeDir:" + OutSavePath)
		MakeDir OutSavePath
		--Convert txt To dfs
print ("Launch DSFTool.exe:" + InDsfTxtFile + " to " + OutSavePath)
		command = ""
		command += ("\"" + (pathConfig.removePathLeaf (getThisScriptFilename())) + "\\\\DSFTool.exe" + "\"")
		command += " --text2dsf "
		command += ("\"" + InDsfTxtFile + "\"")  
		command += (" ")  
		command += ("\"" + OutSavePath + (getFilenameFile InDsfTxtFile) + ".dsf" + "\"")
		command += "\n"
		--command += "Pause"
		ShellLaunch  (PBWinControll.WriteBatFile ((pathConfig.removePathLeaf InDsfTxtFile) + "\\Convert" + (getFilenameFile InDsfTxtFile) + ".bat") command True) ""

		Return ((OutSavePath + (getFilenameFile InDsfTxtFile)) + ".dsf")
	)
)