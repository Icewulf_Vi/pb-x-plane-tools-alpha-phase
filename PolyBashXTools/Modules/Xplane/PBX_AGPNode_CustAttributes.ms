PBX_AGPNode_CustAttributes = attributes "APGNode_Xplane_CustAtt"
(
	Parameters main rollout:params
	(
		SitOnGround Type:#Boolean ui:SitOnGround
		X Type:#Integer
		Y Type:#Integer
		U Type:#Float
		V Type:#Float
		RenderOAOnGround Type:#Boolean
	)
	
	Rollout Params "X Plane Properties"
	(
		checkbox SitOnGround "Sit on Ground" state:($.SitOnGround) triState:2
	)
)