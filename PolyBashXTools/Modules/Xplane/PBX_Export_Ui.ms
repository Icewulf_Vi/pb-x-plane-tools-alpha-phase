rollout ExportToXplaneUi "Xplane ExportData"
(
	local PropClass = PBXPropStr
	local pProjektpath = (PropClass.getFileProp "XPlaneExportPath")
	local pExportObjects = (PropClass.getFileProp "XPExportObjects" AsBoolean:true)
	local pExportLIN = (PropClass.getFileProp "XPExportLIN" AsBoolean:true)
	local pExportAGP = (PropClass.getFileProp "XPExportAGP" AsBoolean:true)
	local pExportPOL = (PropClass.getFileProp "XPExportPOL" AsBoolean:true)
	local pExportDSF = (PropClass.getFileProp "XPExportDSF" AsBoolean:true)
	
	editText edtOutputPath "Output Path" pos:[15,5] width:350 height:20 text:pProjektpath readOnly:True
	
	button gOutP "Get it" pos:[380,5] width:49 height:20

	on gOutP pressed do
	(
		Projektpath = getSavePath caption:"Export Path" initialDir:"C:/Your Xplane Scenery"
		if Projektpath == undefined then edtOutputPath.text = "C:/Your Xplane Scenery"
		else
		(
			edtOutputPath.text = (Projektpath + "\\")
			PropClass.SetFileProp "XPlaneExportPath" (Projektpath + "\\") as string
		)
	)
	
	CheckBox CbExportObjects "Export Objects" checked:pExportObjects
	on CbExportObjects changed arg do PropClass.SetFileProp "XPExportObjects" arg
		
	CheckBox CbExportLin "Export Lin" checked:pExportLin
	on CbExportLin changed arg do PropClass.SetFileProp "XPExportLin" arg
		
	CheckBox CbExportPol "Export POL" checked:pExportPol
	on CbExportPol changed arg do PropClass.SetFileProp "XPExportPol" arg
	
	CheckBox CbExportAGP "Export AGP" checked:pExportAGP
	on CbExportAGP changed arg do PropClass.SetFileProp "XPExportAGP" arg
	
	CheckBox CbExportDSF "Export DSF" checked:pExportDSF
	on CbExportDSF changed arg do PropClass.SetFileProp "XPExportDSF" arg
	
	button BtExportAll "ExportScene"
	
	Progressbar PbStatus "" value:0.0
	
	on BtExportAll pressed do
	(
		PbStatus.value = 0.0
		if CbExportAGP.checked do FileIn ((getFilenamePath (getThisScriptFilename())) + "PBX_ExportAGP.ms")

		PbStatus.value = 10.0
		if CbExportDSF.checked do FileIn ((getFilenamePath (getThisScriptFilename())) + "PBX_ExportDsf.ms")

		PbStatus.value = 20.0
		createDialog PBXMapQueue
		
		if CbExportObjects.checked do FileIn ((getFilenamePath (getThisScriptFilename())) + "PBX_ExportObj.ms")

		PbStatus.value = 30.0
		if CbExportLin.checked do FileIn ((getFilenamePath (getThisScriptFilename())) + "PBX_ExportLin.ms")

		PbStatus.value = 40.0
		if CbExportPol.checked do FileIn ((getFilenamePath (getThisScriptFilename())) + "PBX_ExportPol.ms")

		
		

		PbStatus.value = 100.0
	)
)
