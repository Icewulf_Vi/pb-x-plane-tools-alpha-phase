PBX_SOBJNode_CustAttributes = attributes "PBXSOBJ_Xplane_CustAtt"
(
Parameters main rollout:params
	(
		SitOnGround Type:#Boolean ui:CbSitOnGround
		UseLights Type:#Boolean
		UseGlobals Type:#Boolean ui:CBUseGlobals
		
		WriteObj Type:#Boolean Ui:cbWriteObj default:true
		WriteDSFPlacement Type:#Boolean Ui:CbWriteIntoDSF default:true
		
		useNoBlend Type:#Boolean ui:cbUseNoBlend
		NoBlendValue Type:#Float ui:SpNoBlend
		
		

	)
	
	Rollout Params "X Plane Properties"
	(
		
		local XPSOBJPropClass = PBXPropStr CFGFilePath:(PBXToolsInstallationPath + "\\Modules\\Xplane\\Xplane.cfg")
	

		CheckBox CbWriteIntoDSF "WriteInDsf"
		
		Checkbox cbWriteObj "Export OBJ"

		checkbox CbSitOnGround "Sit on Ground" state:(SitOnGround) triState:2 enabled:WriteObj

		Checkbox CbUseGlobals "Use Global Values" tristate:2 enabled:WriteObj
		
		----------- No Blend ---------------
		Checkbox CbUseNoBlend "Use NoBlend"
		Spinner SpNoBlend "Clamp Value" range:[0,1,0.5] scale:0.01 Type:#Float enabled:(useNoBlend)
		on CbUseNoBlend changed arg do SpNoBlend.enabled = arg
		--------------------------------------------
		
		on cbWriteObj changed arg do
		(
			CbSitOnGround.enabled = arg
			SpNoBlend.enabled = (arg and useNoBlend)

		)

	)
)