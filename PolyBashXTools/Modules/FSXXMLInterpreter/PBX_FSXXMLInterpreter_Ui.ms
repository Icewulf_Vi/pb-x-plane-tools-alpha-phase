
FileIn  ((getFilenamePath (getThisScriptFilename())) + "PBX_FSXXMLInterpreter.ms")

rollout FSXXMLInterpreter "FSX XML Interpreter" width:466 height:800
(
	Local XMLData = #()
	local MaxFileData = #()
	
	button 'btAddXML' "Add XMLFiles" pos:[10,9] width:230 height:25 align:#left
	listbox 'lbMaxFiles' "Max Files" pos:[280,35] width:170 height:25 align:#left
	
	listbox 'lbXmlPlacements' "XML Placements" pos:[10,36] width:230 height:25 align:#left
	button 'btAdd3DMaxFile' "Add 3D MaxFiles" pos:[280,8] width:170 height:25 align:#left	

	button 'bTestGuidMatch' "Match by GUID" pos:[10,391] width:115 height:32 align:#left
	button 'bTestNameMatch' "Match by Name" pos:[125,391] width:115 height:32 align:#left
	
	button 'BtImport' "Import into scene" pos:[280,391] width:170 height:32 align:#left
	
	EditText EtGUID "GUID"
	edittext 'EtFriendlyName' "FriendlyName"
	
	EditText EtLat "Lat:"
	EditText EtLon "Lon:"
	EditText EtAlt "Alt:"
	checkbox 'CbIsAlignToGround' "AlignToGround" 
	
	edittext EtPitch "Pitch"
	edittext 'EtBank' "Bank" 
	edittext 'EtHeading' "Heading" 
	edittext EtScale "Scale"
	
	checkbox 'CbNoShadow' "NoShadow" 
	checkbox 'CbNoCrash' "NoCrash"
	checkbox 'CbNoAutogenSuppression' "NoAutogenSuppression" 

	edittext 'EtImageComplexity' "ImageComplexity" 
	
	edittext 'EtEffectName' "EffectName" 
	edittext 'EtEffectParams' "EffectParams" 
	checkbox 'CbShowOnNight' "ShowOnNight" 
	checkbox 'CbShowOnDay' "ShowOnDay" 
	checkbox 'CbShowOnDusk' "ShowOnDusk" 
	checkbox 'CbShowOnDawn' "ShowOnDawn" 
	
	-- Ui Updater
	Fn UpDateXMLItemDisplay XMLDataID =
	(
		arg = XMLDataID
		if XMLData[arg].Lat != undefined then EtLat.Text = XMLData[arg].Lat 
			else EtLat.Text =  "NotSet"
		
		if XMLData[arg].Lon != undefined then EtLon.Text = XMLData[arg].Lon 
			else EtLon.Text = "NotSet"
		
		if XMLData[arg].Alt != undefined then EtAlt.Text = XMLData[arg].Alt 
			else EtAlt.Text =  "NotSet"
		
		if XMLData[arg].GUID != undefined then EtGUID.Text = XMLData[arg].GUID 
			else EtGUID.Text = "NotSet"
	
		if XMLData[arg].Pitch != undefined then EtPitch.Text = XMLData[arg].Pitch as string
			else EtPitch.Text = "NotSet"
		
		if XMLData[arg].Bank != undefined then EtBank.Text = XMLData[arg].Bank  as string
			else EtBank.Text = "NotSet"
		
		if XMLData[arg].Heading != undefined then EtHeading.Text = XMLData[arg].Heading  as string
			else EtHeading.Text = "NotSet"
		
		if XMLData[arg].FriendlyName != undefined then EtFriendlyName.Text = XMLData[arg].FriendlyName 
			else EtFriendlyName.Text = "NotSet"
		
		if XMLData[arg].ImageComplexity != undefined then EtImageComplexity.Text = XMLData[arg].ImageComplexity 
			else EtImageComplexity.Text = "NotSet"
		
		if XMLData[arg].ObjectScale != undefined then EtScale.Text = XMLData[arg].ObjectScale  as string
			else EtScale.Text =  "NotSet"
		
		if XMLData[arg].EffectName != undefined then EtEffectName.Text = XMLData[arg].EffectName 
			else EtEffectName.Text = "NotSet"
		
		if XMLData[arg].EffectParams != undefined then EtEffectParams.Text = XMLData[arg].EffectParams 
			else EtEffectParams.Text = "NotSet"
		
		
		if XMLData[arg].ShowEffectDay !=undefined then CbShowOnDay.checked = XMLData[arg].ShowEffectDay
			else CbShowOnDay.checked = false
		
		if XMLData[arg].ShowEffectNight !=undefined then CbShowOnNight.checked = XMLData[arg].ShowEffectNight
			else CbShowOnNight.checked = false
		
		if XMLData[arg].ShowEffectDusk !=undefined then CbShowOnDusk.checked = XMLData[arg].ShowEffectDusk
			else CbShowOnDusk.checked = false
		
		if XMLData[arg].ShowEffectDawn !=undefined then CbShowOnDawn.checked = XMLData[arg].ShowEffectDawn
			else CbShowOnDawn.checked = false
		
		
		if XMLData[arg].NoShadow != undefined then CbNoShadow.checked = XMLData[arg].NoShadow 
			else CbNoShadow.checked = false
		
		if XMLData[arg].IsAlignToGround != undefined then CbIsAlignToGround.checked  = XMLData[arg].IsAlignToGround 
			else CbIsAlignToGround.checked = false
		
		if XMLData[arg].NoCrash != undefined then CbNoCrash.checked = XMLData[arg].NoCrash 
			else CbNoCrash.checked = false
		
		if XMLData[arg].NoAutogenSuppression != undefined then CbNoAutogenSuppression.checked = XMLData[arg].NoAutogenSuppression 
			else CbNoAutogenSuppression.checked = false
	)
	
	Fn UpdateXMLPlacementList =
	(
		tmp = #()
		for f in XMLData do append tmp (f.GetListBoxName ())
		lbXmlPlacements.items = tmp
	)
	
	Fn UpdateMaxFilesList =
	(
		tmp = #()
		for f in MaxFileData do append tmp (f.GetListBoxName ())
		lbMaxFiles.items = tmp
	)
	
	-- Ui Interaction
	on btAddXML pressed do
	(
		join XMLData (LoadXMLFiles())
		UpdateXMLPlacementList ()
	)
	on lbXmlPlacements selected arg do
	(
		if XMLData[arg].FSX3DObject != undefined then lbMaxFiles.selection = FindItem MaxFileData XMLData[arg].FSX3DObject
		else lbMaxFiles.selection = 0
		
		UpDateXMLItemDisplay arg
	)
	on btAdd3DMaxFile pressed do
	(
		join MaxFileData (LoadMaxFiles())
		UpdateMaxFilesList ()
	)
	on bTestGuidMatch pressed do
	(
		tmp = MatchGUID XMLData MaxFileData
		messagebox (tmp as string + " of " + XMLData.count as string + "Match")
		UpdateXMLPlacementList ()
	)
	
	on bTestNameMatch pressed do
	(
		tmp = MatchName XMLData MaxFileData
		messagebox (tmp as string + " of " + XMLData.count as string + "Match")
		UpdateXMLPlacementList ()
	)
	
	on BtImport pressed do
	(
		ImportFSXSceneryObjectPlacement XMLData
	)
	
)