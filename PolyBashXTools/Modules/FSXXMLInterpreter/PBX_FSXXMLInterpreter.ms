
fn StringToGeoposString value =
(
	if classof value == string do
	(
		tmpvalue = ""
		Cropped =  (filterstring value "  	\"=mM{}")
		for t in Cropped do 
		(
			tmpvalue += t
		)
		
		if tolower tmpvalue[1] == "n" do
		(
			return (trimleft (tolower tmpvalue) "n")
		)
		
		if tolower tmpvalue[1] == "s" do
		(
			tmpvalue[1] = "-"
			return tmpvalue 
		)
		
		if tolower tmpvalue[1] == "e" do
		(
			return (trimleft (tolower tmpvalue) "e")
		)
		
		if tolower tmpvalue[1] == "w" do
		(
			tmpvalue[1] = "-"
			return tmpvalue 
		)
		
		if tolower tmpvalue[tmpvalue.count] == "m" do
		(
			
			tmpvalue = (trimright (tolower tmpvalue) "m")
			if tmpvalue == "0" do return "0.0"
			return tmpvalue
		)
		if tmpvalue == "0" do return "0.0"
		return tmpvalue 
	)
	
	return undefined
)

fn ModGuid GUID =
(
	if  classof GUID == string do
	(
		tmpGuid = ""
		GUID = tolower GUID
		Cropped =  (filterstring GUID " \"={}")
		for t in Cropped do 
		(
			tmpGuid += t
		)
		if tmpGuid != "" then return tmpGuid else return undefined
	)
)

fn StringToFloat value =
(
	if classof value == string do
	(
		tmpvalue = ""
		Cropped =  (filterstring value " \"={}")
		for t in Cropped do 
		(
			tmpvalue += t
		)
		if tmpGuid != "" do 
		(	
			try (return tmpvalue as float ) catch ()
		)
	)
	if classof value == float do return value
	if classof value == integer do return value as float
	return undefined
)

fn StringToBool value =
(
	if classof value == BooleanClass do return value
	if classof value == String do
	(
		tmp = filterstring value " \""
		for t in tmp do 
		(
			if tolower t == "true" do return true
			if tolower t == "false" do return false
		)
	)
	return undefined
)

Struct FSXSceneryObjectPlacement 
(
	FSX3DObject = undefined,
	
	FileName = undefined,
	
	Lat = undefined,
	
	Lon = undefined,
	
	Alt = undefined,
	
	GUID = undefined,
	fn SetGuid InGUID = (GUID = ModGuid InGUID),
	
	Pitch=undefined,
	fn SetPitch invalue = (Pitch = StringToFloat invalue),
	
	Bank=undefined,
	fn SetBank invalue = (Bank = StringToFloat invalue),

	Heading=undefined,
	fn SetHeading invalue = (Heading = StringToFloat invalue),
	
	FriendlyName = undefined,
	
	ImageComplexity =undefined,
	
	ObjectScale = undefined,
	fn SetObjectScale invalue = (ObjectScale = StringToFloat invalue),
	
	EffectName=undefined,
	
	EffectParams=undefined,
	
	ShowEffectDay=undefined,
	fn SetShowEffectDay value = ( ShowEffectDay = StringToBool value ),
	
	ShowEffectNight=undefined,
	fn SetShowEffectNight value = ( ShowEffectNight = StringToBool value ),
	
	ShowEffectDusk=undefined,
	fn SetShowEffectDusk value = ( ShowEffectDusk = StringToBool value ),
	
	ShowEffectDawn=undefined,
	fn SetShowEffectDawn value = ( ShowEffectDawn = StringToBool value ),
	
	NoShadow = undefined,
	
	IsAlignToGround = undefined,
	fn SetIsAlignToGround value = ( IsAlignToGround = StringToBool value ),
	
	NoCrash = undefined,
	
	NoAutogenSuppression = undefined,  

	fn GetListBoxName =
	(
		if FSX3DObject != undefined do 
		(
			return (FileName +"< >"+ FSX3DObject.FileName)
		)
		
		if GUID != undefined and FileName != undefined and EffectName != undefined do
		(
			return (FileName +"<FX>"+ EffectName )
		)
		
		if GUID != undefined and FileName != undefined  do
		(
				return (FileName +" "+GUID)
		)
		
		if FileName != undefined  do
		(
				return (FileName)
		)
		return "No Info"
	)
)

Struct FSX3DObject 
(
	GUID =undefined,
	FileName = "",
	FileNamePath = "",
	
	fn SetGuid InGUID =
	(
			if  classof InGUID == string do
			(
				GUID = ModGuid InGUID
			)
			
	),
	
	fn GetListBoxName =
	(
		return (FileName)
	)
)

fn LoadXMLFile XMLFile =
(
	local FileString = ""
	
	oFile = openfile XMLFile mode:"r"
	if oFile != undefined then
	(
		seek oFile 0 -- springe an anfang der file
		while eof oFile == False do -- durch datei loopen wenn datei zuende aufh�ren
		(
			TextLine = readline oFile errorAtEOF:false
			FileString += " " + TextLine
		)
		FileString += "!EOF!"
	)
	close oFile
	gc()
	
	OutData = #()

	XmlData = Filterstring FileString "	 >"
	tmp = undefined
	for i = 1 to XmlData.count do
	(
		--print (XmlData[i])
		-- OBJCall --
		if (tolower XmlData[i]) == "<sceneryobject" do
		(
			tmp = FSXSceneryObjectPlacement ()
			tmp.FileName = (getfilenameFile XMLFile)
		)
		
		if tmp != undefined do
		(
			local data = FilterString XmlData[i] "= "

			local flag = tolower data[1]
			local value = data[2]
			if flag == "lat" do  tmp.Lat = StringToGeoposString value
			if flag == "lon" do  tmp.Lon =  StringToGeoposString value
			if flag == "alt" do  tmp.Alt =  StringToGeoposString value
			
			if flag == "pitch" do  tmp.SetPitch value
			if flag == "bank" do  tmp.SetBank value
			if flag == "heading" do  tmp.Setheading value
			if flag == "scale" do tmp.SetObjectScale value
			
			if flag == "instanceid" do
			(
				tmp.SetGUID value
			)
			
			if flag == "name" do
			(
				tmp.SetGUID value
			)
			
			
			if flag == "altitudeisagl" do
			(
				tmp.SetIsAlignToGround value
			)
			
			if flag == "imagecomplexity" do tmp.ImageComplexity = value
			
			if flag == "effectname" do tmp.effectname = value
				
			if flag == "effectparams" do 
			(
				tmpString = ""
				for f = 2 to data.count do tmpString+= data[f]
				effectParms = filterstring tmpString ";/\""
				for ep in effectParms do
				(
					If tolower ep == "day1" do tmp.SetShowEffectDay true
					If tolower ep == "night1" do tmp.SetShowEffectNight true
					If tolower ep == "dusk1" do tmp.SetShowEffectDusk true
					If tolower ep == "dawn1" do tmp.SetShowEffectDawn true
					
					If tolower ep == "day0" do tmp.SetShowEffectDay false
					If tolower ep == "night0" do tmp.SetShowEffectNight false
					If tolower ep == "dusk0" do tmp.SetShowEffectDusk false
					If tolower ep == "dawn0" do tmp.SetShowEffectDawn false
				)
			)
			
			flag = undefined
			value = undefined
			
			if (tolower (XmlData[i] as string)) == "<nocrash/" do tmp.NoCrash = true
			if (tolower (XmlData[i] as string)) == "<noautogensuppression/" do tmp.NoAutogenSuppression =  true
			if (tolower (XmlData[i] as string)) == "<noshadow/" do tmp.NoShadow =  true
			
			if (tolower XmlData[i]) == "</sceneryobject" do
			(
				append OutData (copy tmp)
				tmp = undefined
			)
		)
	)
	return OutData
)

Fn LoadXMLFiles =
(

	theDialog = dotNetObject "System.Windows.Forms.OpenFileDialog"
	theDialog.title = "PLEASE Select One Or More Files"
	theDialog.Multiselect = true
	theDialog.Filter = "XML Files (*.xml)|*.xml" 
	theDialog.FilterIndex = 1 --set the filter drop-down list to All Files
	result = theDialog.showDialog()
	result.ToString()
	result.Equals result.OK
	result.Equals result.Cancel
	tmpXmlFiles = theDialog.fileNames --the selected filenames will be returned as an array
	
	outData = #()
	for XMLFile in tmpXmlFiles do
	(
		if DoesFileExist XMLFile == true do
		(
			join outData (LoadXMLFile XMLFile)
		)
	)
	return outData
)

Fn LoadMaxFiles =
(
	theDialog = dotNetObject "System.Windows.Forms.OpenFileDialog"
	theDialog.title = "PLEASE Select One Or More Files"
	theDialog.Multiselect = true
	theDialog.Filter = "XML Files (*.max)|*.max" 
	theDialog.FilterIndex = 1 --set the filter drop-down list to All Files
	result = theDialog.showDialog()
	result.ToString()
	result.Equals result.OK
	result.Equals result.Cancel
	tmpMaxFiles = theDialog.fileNames --the selected filenames will be returned as an array
	
	
	mfpath = (maxFilePath + maxFileName + ".tmp")
	saveMaxfile mfpath
	undo off
	(
		with redraw off(
			OutData = #()
			for File in tmpMaxFiles do
			(
				tmp = FSX3DObject ()

				tmp.FileNamePath = File
				tmp.FileName = (getfilenameFile File)
				
				loadMaxFile File quiet:true
				try
				(
					tmp.SetGUID (fileproperties.getPropertyValue #custom (fileproperties.findproperty #Custom "GUID"))
				)
				catch
				(
					tmp.GUID = undefined
					print ((getfilenamefile File) + " has no GUID in file")
				)
				
				append OutData tmp
			)
		)
	)
	loadMaxFile  mfpath
	
	return OutData
)

Fn MatchGUID FSXSceneryObjectPlacements FSX3DObjects =
(
	-- DoubleLoop
	MatchCounter = 0
	for FSXSceneryObjectPlacement in FSXSceneryObjectPlacements do
	(
		maxtoParse = FSX3DObjects.count
		for i = 1 to maxtoParse do
		(
			FSX3DObject = FSX3DObjects[i]
			if FSX3DObject.GUID != undefined and FSXSceneryObjectPlacement.GUID != undefined do
			(
				GuidMatch = False
				
			--	print ("compare:"  + FSXSceneryObjectPlacement.GUID + " <-> " + FSX3DObject.GUID)
				if FSXSceneryObjectPlacement.GUID == FSX3DObject.GUID do
				(
					MatchCounter += 1
					FSXSceneryObjectPlacement.FSX3DObject = FSX3DObject
					i = maxtoParse
				)
			)
		)
	)
	return MatchCounter
)

Fn MatchName FSXSceneryObjectPlacements FSX3DObjects =
(
	-- DoubleLoop
	MatchCounter = 0
	for FSXSceneryObjectPlacement in FSXSceneryObjectPlacements do
	(
		maxtoParse = FSX3DObjects.count
		for i = 1 to maxtoParse do
		(
			FSX3DObject = FSX3DObjects[i]
			if FSX3DObject.FileName != undefined and FSXSceneryObjectPlacement.FileName != undefined do
			(
				GuidMatch = False
				
			--	print ("compare:"  + FSXSceneryObjectPlacement.GUID + " <-> " + FSX3DObject.GUID)
				if FSXSceneryObjectPlacement.FileName == FSX3DObject.FileName do
				(
					MatchCounter += 1
					FSXSceneryObjectPlacement.FSX3DObject = FSX3DObject
					i = maxtoParse
				)
			)
		)
	)
	return MatchCounter
)

fn ImportMaxFile filenamePath Name:"NoName" =
(
		Print ("Merge Data from " + filenamePath)
		mergeMaxFile filenamePath #select 
			
		theMesh = group $
		theMesh.name = Name
		
		theMesh.pivot = [0,0,0]
	
		return theMesh
)

fn ImportFSXSceneryObjectPlacement FSXSceneryObjectPlacements EntrysWithoutFilesAsDummy:True =
(
	PropClass = PBXPropStr ()

	GeoClass = PBGeoCordStr \
	WorldZeroCordLat:(PropClass.getFileProp "WorldZeroCordLat") \
	WorldZeroCordLon:(PropClass.getFileProp "WorldZeroCordLon") \
	WorldZeroCordAlt:(PropClass.getFileProp "WorldZeroCordAlt")
	
	
	for Placement in FSXSceneryObjectPlacements do
	(
		Geopos = PBGeographicPosition \
		Lat:(Placement.Lat) \
		Lon:(Placement.Lon) \
		Alt:(Placement.Alt)
		print Geopos
		theObjectPos = GeoClass.GetMaxPos Geopos
		
		if theObjectPos != undefined then
		(
			-- load3d data
			-- if needed make dummy
			if  Placement.FSX3DObject != undefined then
			(
				theObject = ImportMaxFile Placement.FSX3DObject.FileNamePath
			)
			else
			(
				if EntrysWithoutFilesAsDummy do
				(
					theObject = Box length:5 height:5 width:5
				)
			)
			
			--Move to geopos, becareful with the shifting pivot
			theObject.Pos += theObjectPos
			if Placement.Heading != undefined then
			(
				theObject.Rotation.Z_Rotation = Placement.Heading as float
			)
			else
			(
				print "Error Heading wrong"
			)
			
		)
		else print "Error at geopos calculation, no object imported"
	)
)