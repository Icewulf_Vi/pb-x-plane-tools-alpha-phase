fn PBX_ConvertFSXMaterialToStandard obj =
(
	matarray = #()
	newmatarray = #()
	
	if classof obj.material == FSXMaterial do append matarray obj.material
	if classof obj.material == Multimaterial do
	(
		for i = 1 to obj.material.count do
		(
			if (classof obj.material[i]) == FSXMaterial do append matarray obj.material[i]
		)
	)
	
	for i = 1 to matarray.count do print ("found " + matarray[i] as string)
	
	for i = 1 to matarray.count do
	(
		m = matarray[i] -- loads one material
		
		newmat = standard name: m.name -- convert mat to standard material
		newmat.maps = m.standard.maps -- get material map slots from old
		newmat.ambient  = m.standard.ambient
		newmat.diffuse = m.standard.diffuse
		newmat.specular = m.standard.specular
		showtexturemap newmat true
		append newmatarray newmat
	)
	
	if classof obj.material == FSXMaterial do obj.material = newmatarray[1]
	if classof obj.material == Multimaterial do
	(
		for i = 1 to obj.material.count do
		(
			obj.material[i] = newmatarray[i]
		)
	)
)