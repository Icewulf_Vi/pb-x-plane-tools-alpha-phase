
FileIn  ((getFilenamePath (getThisScriptFilename())) + "FSXMaterialConverter.ms")

rollout FSXMaterialConverterUi "FSX Material Converter" width:466 height:800
(
	button fsx2std "Convert FSX-Material to Standard" enabled:true
	on fsx2std pressed do
	(
		obj = #()
		join obj selection
		for i = 1 to obj.count do
		(
			undo on
			(
				PBX_ConvertFSXMaterialToStandard obj[i]
			)
		)
	)
)