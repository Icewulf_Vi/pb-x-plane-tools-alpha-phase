
FileIn ((getFilenamePath (getThisScriptFilename())) + "PBX_GoogleMapsImport.ms")

rollout PBX_GoogleMapsImport_Ui "Google Maps Import"
(
	Local Filepath = (maxFilePath + "\\Ressurces\\GoogleMapsDownloads\\")
	edittext PathTextField "Savepath" text:Filepath
	spinner W_segs "X Pictures"  range:[1,22,2] type:#integer scale:1 --x
	spinner L_segs "Y Pictures"  range:[1,22,2] type:#integer scale:1 --y
	spinner G_zoom "Zoom Level"  range:[13,23,19] type:#integer scale:1
	edittext xdem "X dim" text:"307.426m" readOnly:True
	edittext ydem "Y dim" text:"307.426m" readOnly:True
	
	button GenGround "Generate Ground Plane"
	button replaceSel "Replace selection"

	on PathTextField changed arg do Filepath = arg
	
	on L_segs changed i do
	(
		ydem.text = (((fn_CalcGooglePlateSize  G_zoom.value) * i)  as string) + "m"
	)
	
	on w_segs changed i do
	(
		xdem.text = (((fn_CalcGooglePlateSize  G_zoom.value) * i) as string) + "m"
	)
	
	on G_zoom changed i do
	(
		xdem.text = (((fn_CalcGooglePlateSize  i) * w_segs.value) as string) + "m"
		ydem.text = (((fn_CalcGooglePlateSize  i) * L_segs.value) as string) + "m"
	)
	
	on GenGround pressed do
	(
		obj = fn_GenGoolgeGroundpatch #(l_segs.value,w_segs.value) (point3 0 0 0) G_zoom.value Filepath
		if obj == False do (Messagebox "Download Failed")
		if obj == "HTTP 403" then (
			Messagebox "Download limit reached! Wait 24h."
		)
		else
		(
			Messagebox "Finish Loading Picture Files"
			select obj
		)
	)
	
	on replaceSel pressed do
	(
		sel = #()
		join sel selection
		for i = 1 to sel.count do
		(
			obj = fn_GenGoolgeGroundpatch #(l_segs.value,w_segs.value) sel[i].pos G_zoom.value Filepath
			Messagebox ("Finish Loading Picture Files " + i as string + " of " + sel.count as string)
			delete sel[i]
		)

	)
	
	
	
	
)
