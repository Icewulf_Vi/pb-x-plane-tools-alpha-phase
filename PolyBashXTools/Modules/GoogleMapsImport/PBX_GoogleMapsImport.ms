	PropClass = PBXPropStr ()
	MattClass =	PBMaterialAndTexture()
	GeoClass = PBGeoCordStr \
	WorldZeroCordLat:(PropClass.getFileProp "WorldZeroCordLat") \
	WorldZeroCordLon:(PropClass.getFileProp "WorldZeroCordLon") \
	WorldZeroCordAlt:(PropClass.getFileProp "WorldZeroCordAlt") 

fn fn_CalcGooglePlateSize Google_Zoom =
(

	if (GeoClass.WorldZeroCordLat as integer) > 0 then (cosvalue = GeoClass.WorldZeroCordLat as integer) else (cosvalue = GeoClass.WorldZeroCordLat as integer * 1)

	platesize = (cos cosvalue ) * 380 --Gr��e eiener kachel auf den breitengrad 0
		
	if google_zoom == 23 do size = platesize/2/2/2/2/2
	if google_zoom == 22 do size = platesize/2/2/2/2
	if google_zoom == 21 do size = platesize/2/2/2
	if google_zoom == 20 do size = platesize/2/2
	if google_zoom == 19 do size = platesize/2
	if google_zoom == 18 do size = platesize
	if google_zoom == 17 do size = platesize*2
	if google_zoom == 16 do size = platesize*2*2
	if google_zoom == 15 do size = platesize*2*2*2
	if google_zoom == 14 do size = platesize*2*2*2*2
	if google_zoom == 13 do size = platesize*2*2*2*2*2
	return size
)

fn fn_DownloadGooglePicture geocord Texturepath Gzoom = -- downloads airpicture from google / geocord: "54.187837,7.882720" / savepath: "D:\\example\\" /Gzoom: zoom value for google / returns the path of the downloaded file
(
	url = "http://maps.google.com/maps/api/staticmap?center="+ (geocord.Lat +","+geocord.Lon) + "&zoom=" + (Gzoom as string ) + "&size=640x640&maptype=satellite&sensor=false"
	print "#request url#"
	Print url
	filename = ("GoogleAirpicture" + (timeStamp() as string) + ".png")
	
	print ("downloading file for" + (geocord.Lat +","+geocord.Lon)  + "with a zoom of " + (Gzoom as string) + " to " + Texturepath + filename)
	
	Try
	(
		wc = dotNetObject "System.Net.WebClient"
		wc.downloadfile (dotNetObject "System.String" url) (dotNetObject "System.String" (Texturepath + filename))
		return (Texturepath + filename)
	)
	catch 
	(
		if (getCurrentException()) == "-- Runtime error: dotNet runtime exception: The remote server returned an error: (403) Forbidden." do return "HTTP 403"
		
		return False
	)
)

fn fn_GenGoolgeGroundPatch segarray pos Google_zoom Texturepath= --segarray #(length segments as integer,width segments as integer) / pos [Xposition as float,X position as float,Zposotion as float]
(
	-- calc platesize
	size = fn_CalcGooglePlateSize Google_Zoom
	-- make plate	
	googleGroundplane = plane pos:pos isSelected:off name:(uniquename "GoogleAirpicture")
	
	googleGroundplane.width = segarray[2] * size
	googleGroundplane.length = segarray[1] * size
	googleGroundplane.widthsegs  = segarray[2]
	googleGroundplane.lengthsegs = segarray[1]
	
	
	addModifier googleGroundplane (uvwmap maptype: 5)
	ConvertTo googlegroundplane Editable_Poly
	
	-- Download Textures and make material
	makedir Texturepath
	
	googleGroundPlaneMaterial = Multimaterial numsubs:(polyop.getnumfaces googleGroundplane) Name:(Uniquename "GoogleGround")
	googleGroundplane.material = googleGroundPlaneMaterial
	
	for i = 1 to (polyop.getnumfaces googleGroundplane) do
	(
		facecenter = polyop.getFaceCenter googlegroundplane i
		geopos = GeoClass.GetGeoPos  facecenter
		
		polyop.setFaceMatID googlegroundplane #(i) i
		
		texture = fn_DownloadGooglePicture geopos Texturepath google_zoom
		if Texture == False do return False
		if Texture == "HTTP 403" then return "HTTP 403" 
		else
		(
			mat = standardMaterial Name:(Uniquename "GoogleGround_Part")
			mat.diffuseMap = Bitmaptexture fileName:texture
			googleGroundPlaneMaterial[i] = mat
			showTextureMap mat true
		)
	)
	return googleGroundplane
)