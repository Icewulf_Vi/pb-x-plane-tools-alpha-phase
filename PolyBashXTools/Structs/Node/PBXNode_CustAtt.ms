PBXNode_CustAtt = attributes "PBX Node"
(
	--Methodes
	Parameters main rollout:params
	(
		Type Type:#String
		RootID Type:#string -- Id if node is a instance of
		NodeVersion Type:#Integer
		ID Type:#string 
		
	)
	Rollout Params "Node Parameters"
	(
		edittext TextID "ID" text:ID readOnly:True labelOnTop:True
		edittext TextRootID "Root ID" text:RootID labelOnTop:True readOnly:True enabled:(RootID != "")
	)
	
	on create do ID = (PBNaming ()).MakeID ()
	on clone orig do ID = (PBNaming ()).MakeID ()
)