PBXTexture_CustAtt = attributes "PBX Texture"
(
	Parameters main rollout:Params
	(	
		HasNodeTexture Type:#Boolean default:false -- used for "is check in PBXNodeTexture" -- the property is needed not the value
		
		NodeMaterial Type:#Material -- Stores the Material
		TextureSize Type:#String tabSizeVariable:true default:"" -- Stores a string with sizes
		BakeProperties Type:#String tabSizeVariable:true default:"" -- Stores a string with boolean values for diffuse baking
		GrapFromChildren Type:#Boolean default:false
		
		-- Behavier Properties
		ForcedMultiMaterial Type:#Boolean default:false
		ForcedSingleMaterial Type:#Boolean default:true
		
		ForcedBake Type:#Boolean default:False
		CanBakeTexture Type:#Boolean default:true -- Enables the option for Texture baking
		CanGrapFromChildren Type:#Boolean default:False
		
		
	)
	
	Rollout Params "Texture Properties"
	(
	--	Materialbutton MbMaterial "Material" material = PBXNodeTexture.GetMaterial
	--	
		Local MatID = 1
		DropdownList DlMatId "Mat ID" visible:ForcedMultimaterial selection:MatID Items:(PBXNodeTexture.GetMatIdItems $)
		
		Checkbox CbGrapFromCildren "Grap from children" visible:CanGrapFromChildren checked:GrapFromChildren
		MaterialButton MbNodeMaterial NodeMaterial[MatID].Name Material:(PBXNodeTexture.GetMaterial $ Id:MatId) visible:(not ForcedBake) enabled:(not GrapFromChildren)
		
		spinner SpWidth "TextureWidth" range:[2,4096,(PBXNodeTexture.GetSize $).x] scale:2 setKeyBrackets:False type:#Integer
		spinner SpHeight "TextureHeight" range:[2,4096,(PBXNodeTexture.GetSize $).y] scale:2 setKeyBrackets:False type:#Integer
		
		label lbBm "Texture Bake" visible:CanBakeTexture
		
		checkbox cbBakeTexture "BakeTexture" visible:CanBakeTexture checked:(PBXNodeTexture.GetBakeProperties $).EnabledBake enabled:(not ForcedBake)
		
		spinner spMappingID "TargetUVMapping" range:[1,99,(PBXNodeTexture.GetBakeProperties $).MappingChannel] scale:1 setKeyBrackets:False type:#Integer visible:(CanBakeTexture and (not ForcedBake)) enabled:cbBakeTexture.checked

		CheckBox CbBakeDiffuseMap "Diffuse" visible:CanBakeTexture checked:(PBXNodeTexture.GetBakeProperties $).BakeDiffuseMap enabled:cbBakeTexture.checked
			CheckBox CbBakeShadows "Shadow" visible:CanBakeTexture checked:(PBXNodeTexture.GetBakeProperties $).BakeShadowMap enabled:(CbBakeDiffuseMap.checked and cbBakeTexture.checked)
			CheckBox CbBakeLight "Lighting" visible:CanBakeTexture checked:(PBXNodeTexture.GetBakeProperties $).BakeLightMap enabled:(CbBakeDiffuseMap.checked and cbBakeTexture.checked)
		
		CheckBox CbBakeOpacityMap "Opacity" visible:CanBakeTexture checked:(PBXNodeTexture.GetBakeProperties $).BakeOpacityMap enabled:cbBakeTexture.checked
		
		CheckBox CbBakeSpecularMap "Specular" visible:CanBakeTexture checked:(PBXNodeTexture.GetBakeProperties $).BakeSpecularMap enabled:cbBakeTexture.checked
		
		CheckBox CbBakeSelfIllumination "Self-Illumination" visible:CanBakeTexture checked:(PBXNodeTexture.GetBakeProperties $).BakeSelfIllumMap enabled:cbBakeTexture.checked
			checkBox CbSelfIlumIncludeLight "IncludeLightning" visible:CanBakeTexture checked:(PBXNodeTexture.GetBakeProperties $).BakeSelfIllumMapIncludeLights enabled:(CbBakeSelfIllumination.checked  and cbBakeTexture.checked)
			checkBox CbSwitchLights "SwitchLights" visible:CanBakeTexture checked:(PBXNodeTexture.GetBakeProperties $).SwitchLightsOnSelfilum enabled:false -- need more investigation nothing is done jet -- enabled:(CbSelfIlumIncludeLight.checked  and cbBakeTexture.checked)
		
		
		CheckBox CbBakeNormalMap "Normal" visible:CanBakeTexture checked:(PBXNodeTexture.GetBakeProperties $).BakeNormalMap enabled:cbBakeTexture.checked

		
		fn UpdateNodeUI =
		(
			MbNodeMaterial.Material = (NodeMaterial[MatID])
			try
			(
			MbNodeMaterial.caption = MbNodeMaterial.Material.Name
			)
			catch
			()
			DlMatId.visible = ForcedMultimaterial  
			--DlMatId.selection = MatID
			DlMatId.Items = (PBXNodeTexture.GetMatIdItems $)
			
			tmp = PBXNodeTexture.GetSize $ ID:MatID
			SpWidth.value = tmp.x
			SpHeight.value = tmp.y
			
			BakeTmp = PBXNodeTexture.GetBakeProperties $ ID:MatID
			
			--lbBm.visible = CanBakeTexture
			--cbBakeTexture.visible = CanBakeTexture 
			cbBakeTexture.checked = BakeTmp.EnabledBake
		
			spMappingID.value = BakeTmp.MappingChannel
			--spMappingID.visible = CanBakeTexture
			spMappingID.enabled = cbBakeTexture.checked

			--CbBakeDiffuseMap.visible = CanBakeTexture 
			CbBakeDiffuseMap.checked = BakeTmp.BakeDiffuseMap 
			CbBakeDiffuseMap.enabled = cbBakeTexture.checked
			
			--CbBakeShadows.visible = CanBakeTexture 
			CbBakeShadows.checked = BakeTmp.BakeShadowMap
			CbBakeShadows.enabled = (CbBakeDiffuseMap.checked  and cbBakeTexture.checked)
			
			--CbBakeLight.visible = CanBakeTexture 
			CbBakeLight.checked = BakeTmp.BakeLightMap 
			CbBakeLight.enabled = CbBakeDiffuseMap.checked
		
			--CbBakeOpacityMap.visible = CanBakeTexture 
			CbBakeOpacityMap.checked = BakeTmp.BakeOpacityMap 
			CbBakeOpacityMap.enabled = cbBakeTexture.checked
		
			--CbBakeSpecularMap.visible = CanBakeTexture 
			CbBakeSpecularMap.checked = BakeTmp.BakeSpecularMap 
			CbBakeSpecularMap.enabled = cbBakeTexture.checked
		
			--CbBakeSelfIllumination.visible = CanBakeTexture 
			CbBakeSelfIllumination.checked = BakeTmp.BakeSelfIllumMap 
			CbBakeSelfIllumination.enabled = cbBakeTexture.checked
			
			--CbSelfIlumIncludeLight.visible = CanBakeTexture 
			CbSelfIlumIncludeLight.checked = BakeTmp.BakeSelfIllumMapIncludeLights 
			CbSelfIlumIncludeLight.enabled = (CbBakeSelfIllumination.checked  and cbBakeTexture.checked)
			
			--CbBakeNormalMap.visible = CanBakeTexture 
			CbBakeNormalMap.checked = BakeTmp.BakeNormalMap 
			CbBakeNormalMap.enabled = cbBakeTexture.checked
			
			CbGrapFromCildren.checked = GrapFromChildren
			MbNodeMaterial.enabled = (not GrapFromChildren)
		
			--CbSwitchLights.visible = CanBakeTexture 
		--	CbSwitchLights.checked = BakeTmp.SwitchLightsOnSelfilum 
		--	CbSwitchLights.enabled = false -- need more investigation nothing is done jet -- enabled:(CbSelfIlumIncludeLight.checked  and cbBakeTexture.checked)
		)
		
		fn SetBakeProperties =
		(
			BakeTmp = PBXNodeTexture.GetBakeProperties $ ID:MatID
			
			BakeTmp.EnabledBake = cbBakeTexture.checked
			BakeTmp.MappingChannel = spMappingID.value
			BakeTmp.BakeDiffuseMap = CbBakeDiffuseMap.checked 
			BakeTmp.BakeShadowMap = CbBakeShadows.checked
			BakeTmp.BakeLightMap = CbBakeLight.checked
			BakeTmp.BakeOpacityMap = CbBakeOpacityMap.checked 
			BakeTmp.BakeSpecularMap = CbBakeSpecularMap.checked
			BakeTmp.BakeSelfIllumMap = CbBakeSelfIllumination.checked
			BakeTmp.BakeSelfIllumMapIncludeLights = CbSelfIlumIncludeLight.checked
			BakeTmp.BakeNormalMap = CbBakeNormalMap.checked
		--	BakeTmp.SwitchLightsOnSelfilum = CbSwitchLights.checked
			
			PBXNodeTexture.SetBakeProperties $ BakeTmp ID:MatID
			
			UpdateNodeUI ()
		)
		
		on CbGrapFromCildren changed arg do
		(
			PBXNodeTexture.SetGrabFromChildren $ arg
			UpdateNodeUI ()
		)
		
		on MbNodeMaterial picked arg do 
		(
			if PBXNodeTexture.CheckInMaterial $ arg then
			(
				PBXNodeTexture.SetMaterial $ arg id:MatID
				UpdateNodeUI ()
			)
			else
			(
				Messagebox "Material not valid!"
			)
		)

		on DlMatId selected arg do 
		(	
			MatID = DlMatId.selection
			UpdateNodeUI ()
		)
		
		on SpHeight changed arg do
		(
			PBXNodeTexture.SetSize $ (point2 SpWidth.value SpHeight.value) Id:MatID
		)
		on SpWidth changed arg do
		(
			PBXNodeTexture.SetSize $ (point2 SpWidth.value SpHeight.value) Id:MatID
		)
		
		on cbBakeTexture changed arg do SetBakeProperties()
		on spMappingID changed arg do SetBakeProperties()
		on CbBakeDiffuseMap changed arg do SetBakeProperties()
		on CbBakeShadows changed arg do SetBakeProperties()
		on CbBakeLight changed arg do SetBakeProperties()
		on CbBakeOpacityMap changed arg do SetBakeProperties()
		on CbBakeSpecularMap changed arg do SetBakeProperties()
		on CbBakeSelfIllumination changed arg do SetBakeProperties()
		on CbSelfIlumIncludeLight changed arg do SetBakeProperties()
		on CbBakeNormalMap changed arg do SetBakeProperties()
		on CbSwitchLights changed arg do SetBakeProperties()


	)
)