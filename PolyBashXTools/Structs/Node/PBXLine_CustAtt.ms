PBXLine_CustAtt = attributes "PBX Line"
(
	Parameters main rollout:Params
	(	
		NodeData Type:#String tabSizeVariable:true default:""
		LineSize Type:#Float ui:SpSize default:10.0
	)
	Rollout Params "Line Properties"
	(
		Spinner SpSize "Size"
		local currentItem = PBXLineObjectStr.GetItem $ 1
		
		Listbox LbItems "Items" items:(PBXLineObjectStr.GetListBoxItems $)
		Button BtAdd "Add"
		Button BtRemove "Remove"
		Button BtMoveUp "Up"
		on BtMoveUp pressed do
		(
			if LbItems.selection >= 2 and LbItems.selection <= LbItems.items.count do
			(
				PBXLineObjectStr.SwitchItems $ (LbItems.selection - 1) LbItems.selection
				LbItems.items = (PBXLineObjectStr.GetListBoxItems $)
				LbItems.Selection = (LbItems.selection - 1)
			)
		)
		
		button BtMoveDown "Down"
		on BtMoveDown pressed do
		(
			if LbItems.selection >= 1 and LbItems.selection <= (LbItems.items.count - 1) do
			(
				PBXLineObjectStr.SwitchItems $ LbItems.selection (LbItems.selection +1)
				LbItems.items = (PBXLineObjectStr.GetListBoxItems $)
				LbItems.Selection = (LbItems.selection + 1)
			)
		)

		
		on BtAdd pressed do
		(
			PBXLineObjectStr.AddNewItem $
			LbItems.items = PBXLineObjectStr.GetListBoxItems $
			if LbItems.items.count >= 2 do BtRemove.Enabled = true
		)
		
		on BtRemove pressed do
		(
			PBXLineObjectStr.Remove $ LbItems.selection
			LbItems.items = PBXLineObjectStr.GetListBoxItems $
			LbItems.selection = 1
			ItemSelection = 1
			if LbItems.items.count == 1 do BtRemove.Enabled = false
		)
		
		EditText EtDisplayName "Name"
		on EtDisplayName changed arg do 
		(
			currentItem.DisplayName = arg
			PBXLineObjectStr.OverwriteItem $ currentItem LbItems.selection
			LbItems.items = PBXLineObjectStr.GetListBoxItems $
		)
		
		
		on MbEdgeMaterial picked arg do
		(
			$.NodeMaterial[1] = arg
			MbEdgeMaterial.caption = arg.name
			PBXLineObjectStr.OverwriteItem $ currentItem LbItems.selection
			LbItems.items = PBXLineObjectStr.GetListBoxItems $
		)
			
		CheckBox CbUseGp "UseGroundOverlay"
		on CbUseGp changed arg do
		(
			currentItem.EdgeOverlay = arg
			PBXLineObjectStr.OverwriteItem $ currentItem LbItems.selection
		)
		Spinner SpShift "Shift" range:[-1000,1000,0] scale:0.01
		on SpShift changed arg do
		(
			currentItem.Shift = arg
			PBXLineObjectStr.OverwriteItem $ currentItem LbItems.selection
		)
		Spinner SpLeftU "Left Cut" range:[0,1,0] scale:0.01
		on SpLeftU changed arg do
		(
			currentItem.EdgeCutLeft = arg
			PBXLineObjectStr.OverwriteItem $ currentItem LbItems.selection
		)
		Spinner SpRightU "Right Cut" range:[0,1,1] scale:0.01
		on SpRightU changed arg do
		(
			currentItem.EdgeCutRight = arg
			PBXLineObjectStr.OverwriteItem $ currentItem LbItems.selection
		)
		CheckBox CbFill "Fill Area"
		on CbFill changed arg do
		(
			currentItem.FillEdge = arg
			PBXLineObjectStr.OverwriteItem $ currentItem LbItems.selection
		)
		
		on MbFillMaterial picked arg do
		(
			$.NodeMaterial[2] = arg
			MbFillMaterial.Caption = arg.name
			PBXLineObjectStr.OverwriteItem $ currentItem LbItems.selection
			LbItems.items = PBXLineObjectStr.GetListBoxItems $
		)
		Spinner SpUVScale "UV Scale" range:[0,100,1]
		on SpUVScale changed arg do
		(
			currentItem.UvScale = arg
			PBXLineObjectStr.OverwriteItem $ currentItem LbItems.selection
		)
		Spinner SpU "U Shift" range:[-10,10,0]
		on SpU changed arg do
		(
			currentItem.AreaMappingU = arg
			PBXLineObjectStr.OverwriteItem $ currentItem LbItems.selection
		)
		Spinner SpV "V Shift" range:[-10,10,0]
		on SpV changed arg do
		(
			currentItem.AreaMappingV = arg
			PBXLineObjectStr.OverwriteItem $ currentItem LbItems.selection
		)
		Spinner SpUVAngle "Rotation" range:[-360,360,0] scale:0.5
		on SpUVAngle changed arg do 
		(
			currentItem.RotationAngle = arg
			PBXLineObjectStr.OverwriteItem $ currentItem LbItems.selection
		)
		
		on LbItems selected arg do
		(
			currentItem = PBXLineObjectStr.GetItem $ arg
			if currentItem == undefined do return undefined
			-- UpdatingUI
			EtDisplayName.text = currentItem.DisplayName
			SpShift.value = currentItem.Shift
			CbUseGp.State = currentItem.EdgeOverlay
			SpLeftU.value = currentItem.EdgeCutLeft
			SpRightU.value = currentItem.EdgeCutRight
			
			CbFill.state  = currentItem.FillEdge
			SpUVScale.value = currentItem.UvScale
			SpU.value = currentItem.AreaMappingU
			SpV.value = currentItem.AreaMappingV
			SpUVAngle.value = currentItem.RotationAngle
		)
	--	CheckBox CbUseObject "Objects on Edge" 
	--	PickButton PbGetObject "Pick Static Object"
	--	Spinner SPSpacing "Spacing"
	--	Spinner SpEdgeObjectRotationMax "Max Random Rot"
	--	Spinner SpEdgeObjectRotationMin "Min Random Rot"
		
	--	CheckBox CbFillAreaWithObjects "Fill Area with Objects"
	--	Spinner SpFillAmmount
	--	Spinner SpChaos "Chaos"
	--	Spinner SpFillObjectRangeMax "Max Random Rot"
	--	Spinner SpFillObjectRangeMin "Min Random Rot"

	)
)