PBXCapture_CustAtt = attributes "PBX Capture"
(
	Parameters main rollout:Params
	(		
		Targets Type:#String tabSizeVariable:true default:""
		CaptureType Type:#String
	)
	Rollout Params "Capture Properties"
	(
		ListBox LbTargets "Targets" items:(PBXCaptureStr.GetListBoxItems $)
		PickButton PbPickTarget "Add" filter:(PBXCaptureStr.TargetFilter) 
		Button BtRemove "Remove" enabled:(LbTargets.items.count > 0)
		dropdownlist DdLCaptureType "CaptureType" items:(PBXCaptureStr.GetCaptureTypes ())
		
		on PbPickTarget picked arg do
		(
			PBXCaptureStr.AddTarget $ arg
			LbTargets.items = (PBXCaptureStr.GetListBoxItems $)
			if LbTargets.items.count > 0 do BtRemove.enabled = true
		)
		
		on BtRemove pressed do
		(
			if LbTargets.Selection != 0 do
			(
				PBXCaptureStr.RemoveTarget $ LbTargets.Selection
				LbTargets.items = (PBXCaptureStr.GetListBoxItems $)
				if LbTargets.items.count == 0 do BtRemove.enabled = false
			)
		)
		
		on DdLCaptureType selected arg do
		(
			$.CaptureType = DdLCaptureType.selected
		)
	)
)