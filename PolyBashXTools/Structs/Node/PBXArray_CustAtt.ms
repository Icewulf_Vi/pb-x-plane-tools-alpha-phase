PBXArray_CustAtt = attributes "PBX Array"
(
	Parameters main rollout:Params
	(		
		NodeData Type:#String tabSizeVariable:true default:""
	)
	Rollout Params "Array Properties"
	(
		Button BTUpdate "Update" 

		ListBox ItemsLB "Items" items:(PBXArrayObjectStr.GetDisplayNames $)
		pickButton AddItem "Add" filter:PBXArrayObjectStr.FilterPickUsable
		Button DelItem "Delete" filter:PBXArrayObjectStr.FilterPickUsable

		Button modRot "Mod Rotations"
		Button ModPos "ModPositions"
		checkbox EnableMod "Use Mod"
		pickButton AddParticleSystem "Add ParticleSystem" filter:PBXArrayObjectStr.FilterParticleSystem
		checkbox EnableParticleSystem "Use Particle System"

		on BTUpdate pressed do PBXArrayObjectStr.CalculateCoordinates $

		fn updateUI =
		(

			ItemsLB.items = (PBXArrayObjectStr.GetDisplayNames $)

			item = PBXArrayObjectStr.GetItem $ ItemsLB.selection
			if item != false do
			(
				if item.ParticleCapture == true then 
				(	
					EnableParticleSystem.state = true
					AddParticleSystem.enabled = true
				) 
				else 
				(
					EnableParticleSystem.state = false
					AddParticleSystem.enabled = true
				)
				if item.UseModded == true then EnableMod.state = true
				else EnableMod.state = false
			)
		)
		
		on Params open do
		(
			updateUI ()
		)
		
		on AddItem picked obj do
		(
			PBXArrayObjectStr.AddItem $ obj
			updateUI ()
		)

		on DelItem pressed do
		(
			PBXArrayObjectStr.RemoveItem $ ItemsLB.selection
			updateUI ()
		)
		
		on modRot pressed do
		(
			PBXArrayObjectStr.MakeCustomRotationCodeDialog $ ItemsLB.selection
		)
		
		on ModPos pressed do
		(
			PBXArrayObjectStr.MakeCustomPositionCodeDialog $ ItemsLB.selection
		)
		
		on EnableMod changed arg do
		(
			PBXArrayObjectStr.EnableMod $ ItemsLB.selection arg
			updateUI () 
		)
		
		on EnableParticleSystem changed arg do
		(
			PBXArrayObjectStr.EnableParticleCapture $ ItemsLB.selection arg
			updateUI () 
		)
		
		on AddParticleSystem picked arg do
		(
			PBXArrayObjectStr.SetParticleCaptureNode $ ItemsLB.selection arg
		)

		on ItemsLB selected arg do
		(
			updateUI ()
		)
	)
)