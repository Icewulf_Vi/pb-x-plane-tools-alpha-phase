PBXPreview_CustAtt = attributes "PBX Preview"
(
	Parameters main rollout:Params
	(	
		NodeStruct Type:#String default:"NoStructDefined"
	)
	Rollout Params "Preview"
	(
		Button BtPreview "Preview"
		Button BtClear "Clear"
		
		on BtPreview pressed do
		(
			if NodeStruct != "NoStructDefined" do
			(
				try
				(
					execute (NodeStruct + ".MakePreview $")
				)
				catch (Messagebox ("No acess to " + NodeStruct ))
			)
		)
		
		on BtClear pressed do
		(
			if NodeStruct != "NoStructDefined" do
			(
				try
				(
					execute (NodeStruct + ".DeletePreview $")
				)
				catch (Messagebox ("No acess to " + NodeStruct ))
			)
		)
	)
)